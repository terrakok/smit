package com.github.terrakok.smit.core

import android.content.Context
import com.github.terrakok.smit.core.db.SmitDB
import com.russhwolf.settings.AndroidSettings
import com.russhwolf.settings.Settings
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp

class AndroidPlatform(private val ctx: Context) : PlatformSpecific {
    override val deviceName: String = "Android device"
    override fun createHttpClient(): HttpClient = HttpClient(OkHttp)
    override fun createSqlDriver(): SqlDriver = AndroidSqliteDriver(SmitDB.Schema, ctx, "Smit.db")
    override fun createSettings(): Settings = AndroidSettings.Factory(ctx).create("SmitPref")
}