package com.github.terrakok.smit.core

import android.app.Application

actual fun createPlatformSpecific(): PlatformSpecific = AndroidPlatform(Application())