import com.github.terrakok.smit.core.entity.EncryptionAlgorithm;
import com.github.terrakok.smit.core.entity.GuestAccess;
import com.github.terrakok.smit.core.entity.HistoryVisibility;
import com.github.terrakok.smit.core.entity.JoinRule;
import com.github.terrakok.smit.core.entity.Membership;
import com.github.terrakok.smit.core.entity.RoomType;

CREATE TABLE Room(
  id TEXT NOT NULL PRIMARY KEY,
  type TEXT AS RoomType,
  version TEXT,
  creator TEXT,
  name TEXT,
  topic TEXT,
  aliases TEXT,
  updated INTEGER,
  avatarUrl TEXT,
  encryption TEXT AS EncryptionAlgorithm,
  historyVisibility TEXT AS HistoryVisibility,
  joinRule TEXT AS JoinRule,
  guestAccess TEXT AS GuestAccess,
  heroes TEXT
);

selectAllIds:
SELECT Room.id
FROM Room
ORDER BY type DESC, updated DESC;

selectById:
SELECT *
FROM Room
WHERE id = ?;

createNewOrIgnore:
INSERT OR IGNORE INTO Room(id)
VALUES (?);

update:
UPDATE Room
SET type = ?,
    version = ?,
    creator = ?,
    name = ?,
    topic = ?,
    aliases = ?,
    updated = ?,
    avatarUrl = ?,
    encryption = ?,
    historyVisibility = ?,
    joinRule = ?,
    guestAccess = ?,
    heroes = ?
WHERE id = ?;

delete:
DELETE FROM Room
WHERE id = ?;

clear:
DELETE FROM Room;

CREATE TABLE RoomToMember(
    roomId TEXT NOT NULL,
    memberId TEXT NOT NULL,
    membership TEXT AS Membership
);

setRoomMember:
INSERT OR REPLACE INTO RoomToMember(roomId, memberId, membership)
VALUES (?, ?, ?);

getRoomMembers:
SELECT *
FROM RoomToMember JOIN Member ON RoomToMember.memberId = Member.id
WHERE RoomToMember.roomId = ?;

CREATE TABLE RoomToRoom(
    parentId TEXT NOT NULL,
    childId TEXT NOT NULL
);

addRoomChild:
INSERT OR IGNORE INTO RoomToRoom(parentId, childId)
VALUES (?, ?);

getRoomChildrenIds:
SELECT childId
FROM RoomToRoom
WHERE RoomToRoom.parentId = ?;

getRoomWithParentIds:
SELECT DISTINCT childId
FROM RoomToRoom;

deleteChildRoomsConnection:
DELETE FROM RoomToRoom
WHERE RoomToRoom.parentId = ?;