CREATE TABLE TimelineEvent (
  id TEXT NOT NULL PRIMARY KEY,
  type TEXT NOT NULL,
  time INTEGER NOT NULL,
  roomId TEXT NOT NULL,
  senderId TEXT NOT NULL,
  contentJson TEXT NOT NULL,
  stateKey TEXT NOT NULL,
  gapMark TEXT
);

create:
INSERT OR REPLACE INTO TimelineEvent(id, type, time, roomId, senderId, contentJson, stateKey)
VALUES (?,?,?,?,?,?, ?);

setGapMark:
UPDATE TimelineEvent
SET gapMark = ?
WHERE id = ?;

deleteGapMark:
UPDATE TimelineEvent
SET gapMark = NULL
WHERE roomId = ? AND gapMark = ?;

getEvent:
SELECT *
FROM TimelineEvent
WHERE id = ?;

getLastEventId:
SELECT id
FROM TimelineEvent
WHERE roomId = ?
ORDER BY time DESC
LIMIT 1;

getOldestEvent:
SELECT *
FROM TimelineEvent
WHERE roomId = ?
ORDER BY time ASC
LIMIT 1;

getTimelineBefore:
SELECT *
FROM TimelineEvent
WHERE roomId = ? AND time < ?
ORDER BY time DESC
LIMIT :limit;

getTimelineAfter:
SELECT *
FROM TimelineEvent
WHERE roomId = ? AND time > ?
ORDER BY time ASC
LIMIT :limit;

clear:
DELETE FROM TimelineEvent;