package com.github.terrakok.smit.core.gateways

import co.touchlab.kermit.Logger
import com.github.terrakok.smit.core.entity.ServerError
import com.github.terrakok.smit.core.entity.Tokens
import io.ktor.client.call.body
import io.ktor.client.plugins.auth.providers.BearerAuthConfig
import io.ktor.client.plugins.auth.providers.BearerTokens
import io.ktor.client.request.host
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.Url
import io.ktor.http.contentType
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put


@Suppress("FunctionName")
internal fun BearerAuthConfig.BearerTokenHandler(
    storage: Storage,
    onSessionExpired: () -> Unit
) {
    sendWithoutRequest { request ->
        val sessionUrl = storage.currentUserSession?.apiUrl?.let { Url(it) }
        request.host == sessionUrl?.host && !request.url.pathSegments.contains("login")
    }
    loadTokens {
        Logger.v { "Bearer loadTokens" }
        storage.currentUserSession?.let {
            BearerTokens(it.token, it.refreshToken)
        }
    }
    refreshTokens {
        Logger.v { "Bearer refreshTokens" }
        storage.currentUserSession?.let { session ->
            if (oldTokens?.accessToken != session.token) {
                //was updated externally
                return@refreshTokens BearerTokens(session.token, session.refreshToken)
            }

            if (session.refreshToken.isEmpty()) {
                //server doesn't support refresh token
                onSessionExpired()
                return@refreshTokens null
            }

            val tokens: Tokens = try {
                client.post(
                    session.apiUrl + "/_matrix/client/v3/refresh"
                ) {
                    markAsRefreshTokenRequest()
                    contentType(ContentType.Application.Json)
                    setBody(buildJsonObject { put("refresh_token", session.refreshToken) })
                }.body()
            } catch (e: ServerError) {
                //token refreshing has failed
                onSessionExpired()
                return@refreshTokens null
            }

            storage.currentUserSession = session.copy(token = tokens.token, refreshToken = tokens.refreshToken)
            BearerTokens(tokens.token, tokens.refreshToken)
        }
    }
}