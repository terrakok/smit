package com.github.terrakok.smit.core.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ErrorResponse(
    @SerialName("errcode") val code: String,
    @SerialName("error") val message: String?
)

data class ServerError(
    val response: ErrorResponse
) : RuntimeException(response.message)