package com.github.terrakok.smit.core

import com.github.terrakok.smit.core.domain.EventCollector
import com.github.terrakok.smit.core.domain.Repository
import com.github.terrakok.smit.core.domain.Session
import com.github.terrakok.smit.core.entity.AppEvent
import com.github.terrakok.smit.core.entity.ConnectionState
import com.github.terrakok.smit.core.entity.Member
import com.github.terrakok.smit.core.entity.Room
import com.github.terrakok.smit.core.entity.TimelineEvent
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow

class SmitService internal constructor(
    private val session: Session,
    private val eventCollector: EventCollector,
    private val repository: Repository
) {
    val connectionState: StateFlow<ConnectionState> = session.connectionState
    val events: SharedFlow<AppEvent> = eventCollector.events
    val sessionState: StateFlow<Boolean> = session.sessionState
    val currentUserId: String? get() = session.currentUserId

    suspend fun login(host: String, username: String, password: String): String {
        return session.login(host, username, password)
    }

    fun connect() {
        session.connect()
    }

    fun disconnect() {
        session.disconnect()
    }

    fun logout() {
        session.logout()
    }

    suspend fun getDirectRoomIds(): List<String> =
        repository.getDirectRoomIds()

    suspend fun getSpaceRoomIds(spaceId: String?): List<String> =
        repository.getSpaceRoomIds(spaceId)

    suspend fun getRoom(roomId: String): Room? =
        repository.getRoom(roomId)

    suspend fun getMember(memberId: String): Member =
        repository.getMember(memberId)

    suspend fun getTimelineEvent(id: String): TimelineEvent? =
        repository.getTimelineEvent(id)

    suspend fun getLastTimelineEvent(roomId: String): TimelineEvent? =
        repository.getLastTimelineEvent(roomId)

    suspend fun getTimelineBefore(roomId: String, time: Long?, limit: Int): List<TimelineEvent> =
        repository.getTimelineBefore(roomId, time, limit)

    suspend fun getTimelineAfter(roomId: String, time: Long, limit: Int): List<TimelineEvent> =
        repository.getTimelineAfter(roomId, time, limit)

    suspend fun loadJoinedMembersForRoom(roomId: String): List<Member> =
        repository.loadJoinedMembersForRoom(roomId)

    fun convertMediaUrl(rawUrl: String): String = repository.convertMediaUrl(rawUrl)

    companion object {
        val HOSTS = listOf("matrix.org", "mozilla.org", "kde.org", "halogen.city", "envs.net")
    }
}