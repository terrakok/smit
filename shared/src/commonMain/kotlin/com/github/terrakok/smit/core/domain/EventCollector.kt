package com.github.terrakok.smit.core.domain

import com.github.terrakok.smit.core.entity.AppEvent
import com.github.terrakok.smit.core.entity.SmitGlobalScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

internal class EventCollector {
    private val _events = MutableSharedFlow<AppEvent>()
    val events: SharedFlow<AppEvent> = _events.asSharedFlow()

    fun newEvent(event: AppEvent) {
        SmitGlobalScope.launch {
            _events.emit(event)
        }
    }
}