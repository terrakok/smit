package com.github.terrakok.smit.core.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal class TimelineResponse(
    @SerialName("chunk") val chunk: List<ClientEvent>,
    @SerialName("start") val start: String,
    @SerialName("end") val end: String? = null,
    @SerialName("state") val state: List<ClientEvent>? = null
)

@Serializable
internal class MembersResponse(
    @SerialName("chunk") val chunk: List<ClientEvent>
)

@Serializable
internal class MemberInfo(
    @SerialName("displayname") val name: String? = null,
    @SerialName("avatar_url") val avatarUrl: String? = null
)

@Serializable
internal class JoinedMembers(
    @SerialName("joined") val joined: Map<String, MemberInfo>
)