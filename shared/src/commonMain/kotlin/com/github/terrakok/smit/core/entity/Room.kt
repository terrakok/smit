package com.github.terrakok.smit.core.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

data class Room(
    val id: String,
    val type: RoomType?,
    val version: String,
    val creator: String,
    val name: String?,
    val topic: String?,
    val aliases: List<String>,
    val updated: Long,
    val avatarUrl: String?,
    val encryption: EncryptionAlgorithm?,
    val historyVisibility: HistoryVisibility,
    val joinRule: JoinRule,
    val guestAccess: GuestAccess,
    val heroes: List<String>
)

@Serializable
enum class RoomType {
    @SerialName("m.space")
    SPACE
}

@Serializable
enum class HistoryVisibility {
    @SerialName("world_readable")
    WORLD_READABLE,

    @SerialName("shared")
    SHARED,

    @SerialName("invited")
    INVITED,

    @SerialName("joined")
    JOINED
}

@Serializable
enum class Membership {
    @SerialName("invite")
    INVITE,

    @SerialName("join")
    JOIN,

    @SerialName("leave")
    LEAVE,

    @SerialName("ban")
    BAN,

    @SerialName("knock")
    KNOCK
}

@Serializable
enum class GuestAccess {
    @SerialName("can_join")
    CAN_JOIN,

    @SerialName("forbidden")
    FORBIDDEN
}

@Serializable
enum class JoinRule {
    @SerialName("public")
    PUBLIC,

    @SerialName("knock")
    KNOCK,

    @SerialName("invite")
    INVITE,

    @SerialName("private")
    PRIVATE,

    @SerialName("restricted")
    RESTRICTED
}


@Serializable
enum class EncryptionAlgorithm {
    @SerialName("m.megolm.v1.aes-sha2")
    AES_SHA2
}