package com.github.terrakok.smit.core.gateways

import co.touchlab.kermit.Logger
import com.github.terrakok.smit.core.entity.UserSession
import com.russhwolf.settings.Settings
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.reflect.KProperty

internal class Storage(
    private val settings: Settings,
    private val json: Json
) {
    private val boxes = mutableSetOf<Box>()
    var deviceId: String? by Box("device-id")
    var currentUserSession: UserSession? by Box("current-session")
    var lastSyncPoint: String? by Box("last-sync-point")
    var directRooms: Map<String, List<String>>? by Box("direct-room-ids")

    fun clear() {
        settings.clear()
        boxes.forEach { it.reset() }
    }

    private inner class Box(val key: String) {
        private var firstRead: Boolean = true
        private var cachedValue: Any? = null

        init {
            boxes.add(this)
        }

        fun reset() {
            firstRead = true
            cachedValue = null
        }

        inline operator fun <reified T> getValue(thisRef: Any?, property: KProperty<*>): T? {
            if (!firstRead) return cachedValue as? T

            Logger.v("First read value: $key")
            val value = settings.getStringOrNull(key)?.let { str ->
                json.decodeFromString<T>(str)
            }
            firstRead = false
            cachedValue = value
            return value
        }

        inline operator fun <reified T> setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
            cachedValue = value
            firstRead = false
            if (value == null) settings.remove(key)
            else settings.putString(key, json.encodeToString<T>(value))
        }
    }
}