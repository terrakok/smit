package com.github.terrakok.smit.core.gateways

import com.github.terrakok.smit.core.entity.ClientEvent
import com.github.terrakok.smit.core.entity.Filter
import com.github.terrakok.smit.core.entity.JoinedMembers
import com.github.terrakok.smit.core.entity.Member
import com.github.terrakok.smit.core.entity.MemberInfo
import com.github.terrakok.smit.core.entity.MembersResponse
import com.github.terrakok.smit.core.entity.Membership
import com.github.terrakok.smit.core.entity.SyncResponse
import com.github.terrakok.smit.core.entity.TimelineResponse
import com.github.terrakok.smit.core.entity.UserSession
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.contentType
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.encodeToJsonElement
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import kotlinx.serialization.json.put

internal class MatrixApi(
    private val hostUrl: () -> String,
    private val httpClient: HttpClient,
    private val deviceName: String
) {
    private val apiPath = "/_matrix/client/v3"
    private val apiUrl get() = hostUrl() + apiPath

    private val mediaPath = "/_matrix/media/v3"
    private val mediaUrl get() = hostUrl() + mediaPath

    fun convertMediaUrl(rawUrl: String): String {
        if (!rawUrl.startsWith("mxc://")) return rawUrl
        return mediaUrl + "/download/" + rawUrl.substringAfter("mxc://")
    }

    suspend fun login(
        host: String,
        user: String,
        password: String,
        deviceId: String?
    ): UserSession {
        val loginTypePassword = "m.login.password"
        val jsonStr = httpClient.get("https://$host/.well-known/matrix/client").bodyAsText()
        val apiHost = Json.parseToJsonElement(jsonStr).jsonObject
            .getValue("m.homeserver").jsonObject
            .getValue("base_url").jsonPrimitive.content

        val loginFlows = httpClient.get("$apiHost$apiPath/login").body<JsonObject>()
            .getValue("flows").jsonArray
            .map { it.jsonObject.getValue("type").jsonPrimitive.content }

        if (!loginFlows.contains(loginTypePassword)) {
            error("Host '$host' doesn't support authorization with login/password!")
        }

        val loginRequest = buildJsonObject {
            put("type", loginTypePassword)
            put("password", password)
            deviceId?.let { put("device_id", deviceId) }
            put("initial_device_display_name", deviceName)
            //put("refresh_token", true) matrix.org doesn't support 'refresh' call
            put("identifier", buildJsonObject {
                put("type", "m.id.user")
                put("user", user)
            })
        }

        val loginResponse = httpClient.post("$apiHost$apiPath/login") {
            contentType(ContentType.Application.Json)
            setBody(loginRequest)
        }.body<JsonObject>()

        return UserSession(
            userId = loginResponse.getValue("user_id").jsonPrimitive.content,
            token = loginResponse.getValue("access_token").jsonPrimitive.content,
            refreshToken = loginResponse.get("refresh_token")?.jsonPrimitive?.content.orEmpty(),
            homeServer = loginResponse.getValue("home_server").jsonPrimitive.content,
            apiUrl = apiHost,
            deviceId = loginResponse.getValue("device_id").jsonPrimitive.content
        )
    }

    suspend fun logout() {
        httpClient.post("$apiUrl/logout") {
            contentType(ContentType.Application.Json)
            setBody(JsonObject(emptyMap()))
        }
    }

    suspend fun sync(since: String?, filter: Filter?, timeout: Int): SyncResponse =
        httpClient.get("$apiUrl/sync") {
            url.parameters.apply {
                since?.let { append("since", since) }
                filter?.let { append("filter", Json.encodeToString(filter)) }
                append("timeout", timeout.toString())
            }
        }.body()

    suspend fun loadTimeline(
        roomId: String,
        isBackward: Boolean,
        filter: Filter?,
        fromId: String?,
        limit: Int,
        toId: String?
    ): TimelineResponse =
        httpClient.get("$apiUrl/rooms/$roomId/messages") {
            url.parameters.apply {
                append("dir", if (isBackward) "b" else "f")
                filter?.let { append("filter", Json.encodeToString(filter)) }
                fromId?.let { append("from", fromId) }
                append("limit", limit.toString())
                toId?.let { append("to", toId) }
            }
        }.body()

    suspend fun getRoomMembers(
        roomId: String,
        at: String?,
        only: Membership?,
        exclude: Membership?
    ): List<ClientEvent> =
        httpClient.get("$apiUrl/rooms/${roomId}/members") {
            url.parameters.apply {
                at?.let { append("at", at) }
                only?.let { append("membership", Json.encodeToJsonElement(only).jsonPrimitive.content) }
                exclude?.let { append("not_membership", Json.encodeToJsonElement(exclude).jsonPrimitive.content) }
            }
        }.body<MembersResponse>().chunk

    suspend fun getRoomJoinedMembers(
        roomId: String
    ): List<Member> =
        httpClient.get("$apiUrl/rooms/${roomId}/joined_members")
            .body<JoinedMembers>().joined
            .map { (id, info) -> Member(id, info.name, info.avatarUrl) }

    suspend fun getMemberInfo(
        memberId: String
    ): Member =
        httpClient.get("$apiUrl/profile/${memberId}")
            .body<MemberInfo>()
            .let { info -> Member(memberId, info.name, info.avatarUrl) }
}