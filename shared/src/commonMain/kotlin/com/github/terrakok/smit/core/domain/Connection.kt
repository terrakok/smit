package com.github.terrakok.smit.core.domain

import co.touchlab.kermit.Logger
import com.github.terrakok.smit.core.entity.ConnectionState
import com.github.terrakok.smit.core.entity.Filter
import com.github.terrakok.smit.core.entity.SmitDispatchers
import com.github.terrakok.smit.core.entity.SmitGlobalScope
import com.github.terrakok.smit.core.gateways.MatrixApi
import com.github.terrakok.smit.core.gateways.Storage
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

internal class Connection(
    private val storage: Storage,
    private val api: MatrixApi,
    private val serverEventHandler: ServerEventHandler
) {
    private val log = Logger.withTag("Connection")

    private val requestTimeout = 30
    private val pauseFactor = 2
    private var requestPause = 0
    private var job: Job? = null

    private val connection = MutableStateFlow<ConnectionState>(ConnectionState.Disconnected())
    val connectionState: StateFlow<ConnectionState> = connection.asStateFlow()

    fun connect() {
        log.d("connect")
        requestPause = 0

        val currentJob = job
        val currentState = connectionState.value
        if (currentState == ConnectionState.Synchronization || currentState == ConnectionState.Connected) {
            log.e("Connection is active!")
            return
        } else {
            currentJob?.cancel()
        }

        job = SmitGlobalScope.launch(SmitDispatchers.IO) {
            connection.emit(ConnectionState.Connection())
            longRequest()
        }
    }

    fun disconnect() {
        log.d("disconnect")
        job?.cancel()
        SmitGlobalScope.launch { connection.emit(ConnectionState.Disconnected()) }
    }

    private suspend fun longRequest() {
        try {
            log.d("longRequest with pause = $requestPause")
            delay(requestPause * 1000L)

            val lastSyncPoint = storage.lastSyncPoint
            val filter = if (lastSyncPoint == null) Filter.lazyMembers else null

            connection.emit(
                if (lastSyncPoint == null) ConnectionState.Synchronization
                else ConnectionState.Connected
            )

            val response = api.sync(lastSyncPoint, filter, requestTimeout * 1000)
            requestPause = 0
            serverEventHandler.handleServerEvent(lastSyncPoint, response)
            storage.lastSyncPoint = response.nextBatch

            connection.emit(ConnectionState.Connected)
        } catch (e: Exception) {
            log.e("longRequest error = $e")
            if (e !is CancellationException) {
                if (requestPause == 0) {
                    requestPause = 1
                } else {
                    requestPause *= pauseFactor
                }
                connection.emit(ConnectionState.Connection(requestPause, e))
            } else {
                throw e
            }
        }
        longRequest()
    }
}