package com.github.terrakok.smit.core.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class UserSession(
    @SerialName("user_id") val userId: String,
    @SerialName("access_token") val token: String,
    @SerialName("refresh_token") val refreshToken: String,
    @SerialName("home_server") val homeServer: String,
    @SerialName("api_url") val apiUrl: String,
    @SerialName("device_id") val deviceId: String
)

@Serializable
internal data class Tokens(
    @SerialName("access_token") val token: String,
    @SerialName("refresh_token") val refreshToken: String
)