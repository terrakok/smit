package com.github.terrakok.smit.core

import co.touchlab.kermit.Logger
import co.touchlab.kermit.Severity
import com.github.terrakok.smit.core.db.Room
import com.github.terrakok.smit.core.db.RoomToMember
import com.github.terrakok.smit.core.db.SmitDB
import com.github.terrakok.smit.core.domain.Connection
import com.github.terrakok.smit.core.domain.EventCollector
import com.github.terrakok.smit.core.domain.Repository
import com.github.terrakok.smit.core.domain.ServerEventHandler
import com.github.terrakok.smit.core.domain.Session
import com.github.terrakok.smit.core.gateways.BearerTokenHandler
import com.github.terrakok.smit.core.gateways.Cache
import com.github.terrakok.smit.core.gateways.ErrorResponseHandler
import com.github.terrakok.smit.core.gateways.MatrixApi
import com.github.terrakok.smit.core.gateways.Storage
import com.russhwolf.settings.Settings
import com.squareup.sqldelight.EnumColumnAdapter
import com.squareup.sqldelight.db.SqlDriver
import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpResponseValidator
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.auth.Auth
import io.ktor.client.plugins.auth.providers.bearer
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logging
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import io.ktor.client.plugins.logging.Logger as KtorLogger

interface PlatformSpecific {
    val deviceName: String
    fun createHttpClient(): HttpClient
    fun createSqlDriver(): SqlDriver
    fun createSettings(): Settings
}

fun SmitService.Companion.create(
    platform: PlatformSpecific,
    withLogs: Boolean
): SmitService {
    Logger.setMinSeverity(
        if (withLogs) Severity.Verbose else Severity.Error
    )

    val json = Json {
        ignoreUnknownKeys = true
    }

    val settings = platform.createSettings()
    val storage = Storage(settings, json)

    var session: Session? = null
    val httpClient = platform.createHttpClient().configure(
        storage,
        { session?.logout() },
        json,
        withLogs
    )
    val api = MatrixApi(
        { storage.currentUserSession?.apiUrl.orEmpty() },
        httpClient,
        platform.deviceName
    )

    val db = SmitDB(
        platform.createSqlDriver(),
        Room.Adapter(
            EnumColumnAdapter(),
            EnumColumnAdapter(),
            EnumColumnAdapter(),
            EnumColumnAdapter(),
            EnumColumnAdapter()
        ),
        RoomToMember.Adapter(
            EnumColumnAdapter()
        )
    )
    val cache = Cache(db, json)

    val eventCollector = EventCollector()
    val serverEventHandler = ServerEventHandler(cache, eventCollector, storage, json)
    val connection = Connection(storage, api, serverEventHandler)
    val repository = Repository(api, cache, storage, json)
    session = Session(api, storage, connection, cache)

    return SmitService(session, eventCollector, repository)
}

internal fun HttpClient.configure(
    storage: Storage,
    onSessionExpired: () -> Unit,
    json: Json,
    withLogs: Boolean
) = config {
    engine {
        threadsCount = 2
        pipelining = true
    }
    expectSuccess = true
    HttpResponseValidator {
        handleResponseExceptionWithRequest { exception, _ ->
            ErrorResponseHandler(exception)
        }
    }
    install(ContentNegotiation) {
        json(json)
    }
    install(HttpTimeout) {
        connectTimeoutMillis = 60000
        requestTimeoutMillis = 60000
        socketTimeoutMillis = 60000
    }
    install(Auth) {
        bearer { BearerTokenHandler(storage, onSessionExpired) }
    }
    if (withLogs) {
        install(Logging) {
            level = LogLevel.ALL
            logger = object : KtorLogger {
                val log = Logger.withTag("SmitHttpClient")
                override fun log(message: String) {
                    log.v(message.replace("\n", "\n\t"))
                }
            }
        }
    }
}