package com.github.terrakok.smit.core.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal class SyncResponse(
    @SerialName("next_batch") val nextBatch: String,
    @SerialName("account_data") val accountData: AccountData? = null,
    @SerialName("rooms") val rooms: RoomsSync? = null,
)

@Serializable
internal class AccountData(
    @SerialName("events") val events: List<AccountEvent>
)

@Serializable
internal class RoomsSync(
    @SerialName("invite") val invite: Map<String, InvitedRoom>? = null,
    @SerialName("join") val join: Map<String, JoinedRoom>? = null,
    @SerialName("leave") val leave: Map<String, LeftRoom>? = null
)

@Serializable
internal class InvitedRoom(
    @SerialName("invite_state") val state: InviteState
)

@Serializable
internal class LeftRoom(
    @SerialName("state") val state: State
)

@Serializable
internal class InviteState(
    @SerialName("events") val events: List<StripedEvent>
)

@Serializable
internal class JoinedRoom(
    @SerialName("state") val state: State,
    @SerialName("summary") val summary: RoomSummary,
    @SerialName("timeline") val timeline: Timeline
)

@Serializable
internal class RoomSummary(
    @SerialName("m.heroes") val heroes: List<String>? = null,
    @SerialName("m.invited_member_count") val invitedMemberCount: Int? = null,
    @SerialName("m.joined_member_count") val joinedMemberCount: Int? = null
)

@Serializable
internal class State(
    @SerialName("events") val events: List<ClientEvent>
)

@Serializable
internal class Timeline(
    @SerialName("events") val events: List<ClientEvent>,
    @SerialName("prev_batch") val previousBatch: String?,
    @SerialName("limited") val limited: Boolean
)