package com.github.terrakok.smit.core.entity

data class Member(
    val id: String,
    val name: String?,
    val avatarUrl: String?
)
