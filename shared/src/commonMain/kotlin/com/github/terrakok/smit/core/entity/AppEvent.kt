package com.github.terrakok.smit.core.entity

sealed interface AppEvent {
    data class RoomStateChanged(val id: String) : AppEvent
    data class RoomNewMessages(val id: String, val withGap: Boolean) : AppEvent
    data class RoomWasDeleted(val id: String) : AppEvent
}