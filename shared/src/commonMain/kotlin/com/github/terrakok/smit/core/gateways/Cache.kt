package com.github.terrakok.smit.core.gateways

import co.touchlab.kermit.Logger
import com.github.terrakok.smit.core.db.SmitDB
import com.github.terrakok.smit.core.entity.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import com.github.terrakok.smit.core.db.Member as DBMember
import com.github.terrakok.smit.core.db.Room as DBRoom
import com.github.terrakok.smit.core.db.TimelineEvent as DBTimelineEvent

internal class Cache(
    private val db: SmitDB,
    private val json: Json
) {
    private val log = Logger.withTag("Cache")

    @Suppress("FunctionName")
    fun TRANSACTION(actions: (Cache) -> Unit) {
        db.transaction { actions(this@Cache) }
    }

    fun clear() {
        db.roomQueries.clear()
        db.memberQueries.clear()
        db.timelineEventQueries.clear()
    }

    fun createRoom(id: String) {
        db.roomQueries.createNewOrIgnore(id)
    }

    fun deleteRoom(id: String) {
        db.roomQueries.delete(id)
    }

    fun updateRoom(
        id: String,
        type: RoomType? = null,
        version: String? = null,
        creator: String? = null,
        name: String? = null,
        topic: String? = null,
        aliases: List<String>? = null,
        updated: Long? = null,
        avatarUrl: String? = null,
        encryption: EncryptionAlgorithm? = null,
        historyVisibility: HistoryVisibility? = null,
        joinRule: JoinRule? = null,
        guestAccess: GuestAccess? = null,
        heroes: List<String>? = null
    ) {
        val current = db.roomQueries.selectById(id).executeAsOne()
        db.roomQueries.update(
            type ?: current.type,
            version ?: current.version,
            creator ?: current.creator,
            name ?: current.name,
            topic ?: current.topic,
            aliases?.takeIf { it.isNotEmpty() }?.joinToString("\n") ?: current.aliases,
            updated ?: current.updated,
            avatarUrl ?: current.avatarUrl,
            encryption ?: current.encryption,
            historyVisibility ?: current.historyVisibility,
            joinRule ?: current.joinRule,
            guestAccess ?: current.guestAccess,
            heroes?.takeIf { it.isNotEmpty() }?.joinToString("\n") ?: current.heroes,
            id
        )
    }

    fun deleteRoomTopic(id: String) {
        val current = db.roomQueries.selectById(id).executeAsOne()
        db.roomQueries.update(
            current.type,
            current.version,
            current.creator,
            current.name,
            null,
            current.aliases,
            current.updated,
            current.avatarUrl,
            current.encryption,
            current.historyVisibility,
            current.joinRule,
            current.guestAccess,
            current.heroes,
            id
        )
    }

    fun deleteRoomAvatar(id: String) {
        val current = db.roomQueries.selectById(id).executeAsOne()
        db.roomQueries.update(
            current.type,
            current.version,
            current.creator,
            current.name,
            current.topic,
            current.aliases,
            current.updated,
            null,
            current.encryption,
            current.historyVisibility,
            current.joinRule,
            current.guestAccess,
            current.heroes,
            id
        )
    }

    fun getSpaceRoomIds(spaceId: String?): List<String> {
        val all = db.roomQueries.selectAllIds().executeAsList()
        if (spaceId == null) {
            val withParentIds = db.roomQueries.getRoomWithParentIds().executeAsList().toSet()
            return all.minus(withParentIds)
        } else {
            val children = db.roomQueries.getRoomChildrenIds(spaceId).executeAsList().toSet()
            return all.filter { children.contains(it) }
        }
    }

    fun getRoom(id: String): Room? =
        db.roomQueries.selectById(id).executeAsOneOrNull()?.toRoom()

    fun createMember(id: String) {
        db.memberQueries.createNewOrIgnore(id)
    }

    fun updateMember(
        id: String,
        name: String?,
        avatarUrl: String?
    ) {
        db.memberQueries.update(name, avatarUrl, id)
    }

    fun setRoomMember(roomId: String, memberId: String, membership: Membership) {
        db.roomQueries.setRoomMember(roomId, memberId, membership)
    }

    fun addChildToRoom(roomId: String, childId: String) {
        db.roomQueries.addRoomChild(roomId, childId)
    }

    fun getMember(memberId: String): Member? =
        db.memberQueries.selectById(memberId).executeAsOneOrNull()?.toMember()

    fun createTimelineEvent(
        id: String,
        type: String,
        time: Long,
        roomId: String,
        senderId: String,
        contentJson: JsonObject,
        stateKey: String
    ) {
        db.timelineEventQueries.create(id, type, time, roomId, senderId, contentJson.toString(), stateKey)
    }

    fun setGapMark(id: String, mark: String?) {
        db.timelineEventQueries.setGapMark(mark, id)
    }

    fun deleteGapMark(roomId: String, mark: String) {
        db.timelineEventQueries.deleteGapMark(roomId, mark)
    }

    fun getLastEventId(roomId: String) =
        db.timelineEventQueries.getLastEventId(roomId).executeAsOneOrNull()

    fun getOldestEvent(roomId: String) =
        db.timelineEventQueries.getOldestEvent(roomId).executeAsOneOrNull()?.toTimelineEvent()

    fun getTimelineEvent(id: String) =
        db.timelineEventQueries.getEvent(id).executeAsOneOrNull()?.toTimelineEvent()

    //NEW -> OLD
    fun getTimelineBefore(roomId: String, time: Long?, limit: Int): List<TimelineEvent> =
        db.timelineEventQueries.getTimelineBefore(roomId, time ?: Long.MAX_VALUE, limit.toLong())
            .executeAsList()
            .map { it.toTimelineEvent() }

    //OLD -> NEW
    fun getTimelineAfter(roomId: String, time: Long, limit: Int): List<TimelineEvent> =
        db.timelineEventQueries.getTimelineAfter(roomId, time, limit.toLong())
            .executeAsList()
            .map { it.toTimelineEvent() }

    private fun DBRoom.toRoom(): Room? {
        return Room(
            id,
            type,
            version ?: return null, //Room is not initialized
            creator ?: return null, //Room is not initialized
            name.takeIf { !it.isNullOrBlank() },
            topic.takeIf { !it.isNullOrBlank() },
            aliases?.lines().orEmpty().filter { it.isNotBlank() },
            updated ?: return null, //Room is not initialized
            avatarUrl.takeIf { !it.isNullOrBlank() },
            encryption,
            historyVisibility ?: return null, //Room is not initialized
            joinRule ?: return null, //Room is not initialized
            guestAccess ?: return null, //Room is not initialized
            heroes?.lines().orEmpty().filter { it.isNotBlank() }
        )
    }

    private fun DBMember.toMember() = Member(
        id,
        name.takeIf { !it.isNullOrBlank() },
        avatarUrl.takeIf { !it.isNullOrBlank() }
    )

    private fun DBTimelineEvent.toTimelineEvent() = TimelineEvent(
        id,
        type,
        time,
        roomId,
        senderId,
        gapMark,
        stateKey,
        EventContentType.getEventContentSerializer(type)?.let {
            try {
                json.decodeFromString(it, contentJson)
            } catch (e: Exception) {
                log.e("TimelineEvent[$id] content $type error: $contentJson")
                null
            }
        } ?: EventContent.Unknown(json.parseToJsonElement(contentJson) as JsonObject)
    )
}