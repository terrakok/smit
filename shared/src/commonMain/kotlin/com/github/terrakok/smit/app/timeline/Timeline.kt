package com.github.terrakok.smit.app.timeline

import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.extensions.coroutines.CoroutineExecutorScope
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineBootstrapper
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineExecutorFactory
import com.github.terrakok.smit.core.SmitService
import com.github.terrakok.smit.core.entity.*
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.launch

object Timeline {
    private const val PAGE_SIZE = 50

    data class State(
        val items: List<TimelineItem> = emptyList(),
        val loading: Boolean = false,
        val members: Map<String, Member> = emptyMap(),
        internal val noMoreBefore: Boolean = false,
        internal val noMoreAfter: Boolean = true,
    )

    sealed interface Intent {
        object LoadMoreBefore : Intent
        object LoadMoreAfter : Intent
    }

    sealed interface Label {
        data class Error(val e: Exception) : Label
    }

    private sealed interface Action {
        data class Loaded(val items: List<TimelineEvent>) : Action
        data class JoinedMembers(val members: List<Member>) : Action
        object NewItems : Action
        object NewGap : Action
    }

    private sealed interface Msg {
        data class NewItemsBefore(val items: List<TimelineEvent>) : Msg
        data class NewItemsAfter(val items: List<TimelineEvent>) : Msg
        data class NewMembers(val members: List<Member>) : Msg
        object GapAtTheEnd : Msg
        object Progress : Msg
        object Void : Msg
        object ReachBeginning : Msg
        object ReachEnd : Msg
    }

    fun createStore(id: String, factory: StoreFactory, smitService: SmitService): Store<Intent, State, Label> =
        factory.create<Intent, Action, Msg, State, Label>(
            name = "TimelineStore($id)",
            initialState = State(),
            bootstrapper = coroutineBootstrapper {
                launch {
                    val joined = smitService.loadJoinedMembersForRoom(id)
                    dispatch(Action.JoinedMembers(joined))
                }
                launch {
                    val page = smitService.getTimelineBefore(id, null, PAGE_SIZE)
                    dispatch(Action.Loaded(page))
                    smitService.events
                        .filterIsInstance<AppEvent.RoomNewMessages>()
                        .filter { it.id == id }
                        .collect {
                            if (it.withGap) {
                                dispatch(Action.NewGap)
                            } else {
                                dispatch(Action.NewItems)
                            }
                        }
                }
            },
            executorFactory = coroutineExecutorFactory {
                onAction<Action.Loaded> {
                    launch {
                        val page = it.items
                        loadUnknownMembers(smitService, page, this@onAction)
                        dispatch(Msg.NewItemsBefore(page))
                    }
                }
                onAction<Action.JoinedMembers> {
                    dispatch(Msg.NewMembers(it.members))
                }
                onAction<Action.NewGap> {
                    dispatch(Msg.GapAtTheEnd)
                }
                onAction<Action.NewItems> {
                    launch {
                        dispatch(Msg.GapAtTheEnd)
                        if (state.loading || !state.noMoreAfter) return@launch
                        val time = state.items.firstOrNull()?.let {
                            when (it) {
                                is MembersPack -> it.events.first().time
                                is MessagesPack -> it.events.first().time
                                is TimelineEvent -> it.time
                            }
                        } ?: return@launch
                        dispatch(Msg.Progress)
                        try {
                            val page = smitService.getTimelineAfter(id, time, PAGE_SIZE)
                            require(page.isNotEmpty())
                            loadUnknownMembers(smitService, page, this@onAction)
                            dispatch(Msg.NewItemsAfter(page))
                        } catch (e: Exception) {
                            publish(Label.Error(e))
                            dispatch(Msg.Void)
                        }
                    }
                }
                onIntent<Intent.LoadMoreBefore> {
                    launch {
                        if (state.loading || state.noMoreBefore) return@launch
                        val time = state.items.lastOrNull()?.let {
                            when (it) {
                                is MembersPack -> it.events.last().time
                                is MessagesPack -> it.events.last().time
                                is TimelineEvent -> it.time
                            }
                        } ?: return@launch
                        dispatch(Msg.Progress)
                        try {
                            val page = smitService.getTimelineBefore(id, time, PAGE_SIZE)
                            if (page.isNotEmpty()) {
                                loadUnknownMembers(smitService, page, this@onIntent)
                                dispatch(Msg.NewItemsBefore(page))
                            } else {
                                dispatch(Msg.ReachBeginning)
                            }
                        } catch (e: Exception) {
                            publish(Label.Error(e))
                            dispatch(Msg.Void)
                        }
                    }
                }
                onIntent<Intent.LoadMoreAfter> {
                    launch {
                        if (state.loading || state.noMoreAfter) return@launch
                        val time = state.items.firstOrNull()?.let {
                            when (it) {
                                is MembersPack -> it.events.first().time
                                is MessagesPack -> it.events.first().time
                                is TimelineEvent -> it.time
                            }
                        } ?: return@launch
                        dispatch(Msg.Progress)
                        try {
                            val page = smitService.getTimelineAfter(id, time, PAGE_SIZE)
                            if (page.isNotEmpty()) {
                                loadUnknownMembers(smitService, page, this@onIntent)
                                dispatch(Msg.NewItemsAfter(page))
                            } else {
                                dispatch(Msg.ReachEnd)
                            }
                        } catch (e: Exception) {
                            publish(Label.Error(e))
                            dispatch(Msg.Void)
                        }
                    }
                }
            },
            reducer = { msg ->
                when (msg) {
                    Msg.Progress -> copy(loading = true)
                    Msg.Void -> copy(loading = false)
                    Msg.ReachBeginning -> copy(loading = false, noMoreBefore = true)
                    Msg.ReachEnd -> copy(loading = false, noMoreAfter = true)
                    Msg.GapAtTheEnd -> copy(noMoreAfter = false)
                    is Msg.NewMembers -> copy(members = members.plus(msg.members.associateBy { it.id }))
                    is Msg.NewItemsBefore -> copy(items = mergeTimelines(items, packTimeline(msg.items)), loading = false)
                    is Msg.NewItemsAfter -> copy(items = mergeTimelines(packTimeline(msg.items), items), loading = false)
                }
            }
        )

    private suspend fun loadUnknownMembers(
        smitService: SmitService,
        items: List<TimelineEvent>,
        scope: CoroutineExecutorScope<State, Msg, Label>
    ) {
        scope.launch {
            val newIds = items.map { it.senderId }.toSet() - scope.state.members.keys
            newIds.forEach { id ->
                try {
                    scope.dispatch(Msg.NewMembers(listOf(smitService.getMember(id))))
                } catch (e: Exception) {
                    scope.publish(Label.Error(e))
                }
            }
        }
    }

    //todo optimize it!
    private fun packTimeline(events: List<TimelineEvent>): List<TimelineItem> {
        val result = mutableListOf<TimelineItem>()

        var packType: String? = null
        val packed = mutableListOf<TimelineEvent>()
        events.forEach { event ->
            if (packType == null) {
                packType = event.type
                packed.add(event)
            } else if (packType == event.type) {
                if (event.type == EventContentType.ROOM_MEMBER) {
                    packed.add(event)
                } else if (event.type == EventContentType.ROOM_MESSAGE) {
                    val packSenderId = packed.first().senderId
                    if (packSenderId == event.senderId) {
                        packed.add(event)
                    } else {
                        result.add(MessagesPack(packed.toList(), packSenderId))
                        packed.clear()
                        packed.add(event)
                    }
                } else {
                    result.add(packed.single())
                    packed.clear()
                    packed.add(event)
                }
            } else {
                if (packType == EventContentType.ROOM_MEMBER) {
                    result.add(MembersPack(packed.toList()))
                } else if (packType == EventContentType.ROOM_MESSAGE) {
                    val packSenderId = packed.first().senderId
                    result.add(MessagesPack(packed.toList(), packSenderId))
                } else {
                    result.add(packed.single())
                }
                packed.clear()
                packType = event.type
                packed.add(event)
            }
        }


        when (packType) {
            EventContentType.ROOM_MEMBER -> result.add(MembersPack(packed))
            EventContentType.ROOM_MESSAGE -> result.add(MessagesPack(packed, packed.first().senderId))
            else -> result.add(packed.single())
        }

        return result
    }

    private fun mergeTimelines(a: List<TimelineItem>, b: List<TimelineItem>): List<TimelineItem> {
        val lastAItem = a.lastOrNull() ?: return b
        val lastAEvent = when (lastAItem) {
            is TimelineEvent -> lastAItem
            is MessagesPack -> lastAItem.events.last()
            is MembersPack -> lastAItem.events.last()
        }
        val firstBItem = b.firstOrNull() ?: return a
        val firstBEvent = when (firstBItem) {
            is TimelineEvent -> firstBItem
            is MessagesPack -> firstBItem.events.first()
            is MembersPack -> firstBItem.events.first()
        }

        if (lastAEvent.type != firstBEvent.type) {
            return a + b
        } else {
            if (lastAEvent.type == EventContentType.ROOM_MEMBER) {
                lastAItem as MembersPack
                firstBItem as MembersPack
                val mergedItem = MembersPack(lastAItem.events + firstBItem.events)
                return a.take(a.size - 1) + mergedItem + b.takeLast(b.size - 1)
            } else if (lastAEvent.type == EventContentType.ROOM_MESSAGE) {
                lastAItem as MessagesPack
                firstBItem as MessagesPack
                if (lastAItem.senderId != firstBItem.senderId) {
                    return a + b
                } else {
                    val mergedItem = MessagesPack(lastAItem.events + firstBItem.events, lastAItem.senderId)
                    return a.take(a.size - 1) + mergedItem + b.takeLast(b.size - 1)
                }
            } else {
                return a + b
            }
        }
    }
}