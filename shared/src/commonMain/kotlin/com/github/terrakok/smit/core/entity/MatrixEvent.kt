package com.github.terrakok.smit.core.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

@Serializable
internal class AccountEvent(
    @SerialName("type") val type: String,
    @SerialName("content") val content: JsonObject
)

@Serializable
internal class StripedEvent(
    @SerialName("type") val type: String,
    @SerialName("sender") val sender: String,
    @SerialName("state_key") val stateKey: String,
    @SerialName("content") val content: JsonObject
)

@Serializable
internal class ClientEvent(
    @SerialName("type") val type: String,
    @SerialName("room_id") val roomId: String? = null,
    @SerialName("event_id") val id: String,
    @SerialName("origin_server_ts") val serverTime: Long,
    @SerialName("sender") val sender: String,
    @SerialName("state_key") val stateKey: String = "",
    @SerialName("content") val content: JsonObject
)