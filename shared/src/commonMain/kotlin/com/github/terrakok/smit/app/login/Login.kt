package com.github.terrakok.smit.app.login

import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineExecutorFactory
import com.github.terrakok.smit.core.SmitService
import kotlinx.coroutines.launch

object Login {
    data class State(
        val knownHosts: List<String> = emptyList(),
        val selectedHost: String = "",
        val username: String? = null,
        val password: String? = null,
        val isLoading: Boolean = false
    )

    sealed interface Intent {
        data class Auth(val host: String, val username: String, val password: String) : Intent
    }

    sealed interface Label {
        object Success : Label
        data class Error(val e: Exception) : Label
    }

    private sealed interface Action
    private sealed interface Msg {
        data class Start(val host: String, val username: String, val password: String) : Msg
        object Stop : Msg
    }

    fun createStore(factory: StoreFactory, smitService: SmitService): Store<Intent, State, Label> =
        factory.create<Intent, Action, Msg, State, Label>(
            name = "LoginStore",
            initialState = State(SmitService.HOSTS, SmitService.HOSTS.first(), null, null, false),
            executorFactory = coroutineExecutorFactory {
                onIntent<Intent.Auth> { intent ->
                    launch {
                        dispatch(Msg.Start(intent.host, intent.username, intent.password))
                        try {
                            smitService.login(intent.host, intent.username, intent.password)
                            publish(Label.Success)
                        } catch (e: Exception) {
                            publish(Label.Error(e))
                        }
                        dispatch(Msg.Stop)
                    }
                }
            },
            reducer = { msg ->
                when (msg) {
                    is Msg.Start -> copy(
                        selectedHost = msg.host,
                        username = msg.username,
                        password = msg.password,
                        isLoading = true
                    )

                    Msg.Stop -> copy(
                        isLoading = false
                    )
                }
            }
        )
}