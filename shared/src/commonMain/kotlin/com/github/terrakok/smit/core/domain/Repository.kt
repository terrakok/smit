package com.github.terrakok.smit.core.domain

import com.github.terrakok.smit.core.entity.Filter
import com.github.terrakok.smit.core.entity.Member
import com.github.terrakok.smit.core.entity.Membership
import com.github.terrakok.smit.core.entity.Room
import com.github.terrakok.smit.core.entity.SmitDispatchers
import com.github.terrakok.smit.core.entity.TimelineEvent
import com.github.terrakok.smit.core.gateways.Cache
import com.github.terrakok.smit.core.gateways.MatrixApi
import com.github.terrakok.smit.core.gateways.Storage
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json

/**
 * Request-response facade for Matrix data
 */
internal class Repository(
    private val api: MatrixApi,
    private val cache: Cache,
    private val storage: Storage,
    private val json: Json
) {
    suspend fun getSpaceRoomIds(spaceId: String?): List<String> = withContext(SmitDispatchers.IO) {
        val ids = cache.getSpaceRoomIds(spaceId)
        if (spaceId == null) {
            val directIds = storage.directRooms.orEmpty().values.flatten().toSet()
            ids.minus(directIds)
        } else {
            ids
        }
    }

    suspend fun getDirectRoomIds(): List<String> = withContext(SmitDispatchers.IO) {
        val directIds = storage.directRooms.orEmpty().values.flatten().toSet()
        cache.getSpaceRoomIds(null).intersect(directIds).toList()
    }

    suspend fun getRoom(roomId: String): Room? = withContext(SmitDispatchers.IO) {
        cache.getRoom(roomId)
    }

    suspend fun getLastTimelineEvent(roomId: String): TimelineEvent? = withContext(SmitDispatchers.IO) {
        cache.getLastEventId(roomId)?.let {
            cache.getTimelineEvent(it)
        }
    }

    suspend fun getTimelineEvent(id: String): TimelineEvent? = withContext(SmitDispatchers.IO) {
        cache.getTimelineEvent(id)
    }

    suspend fun getTimelineBefore(
        roomId: String,
        time: Long?,
        limit: Int
    ): List<TimelineEvent> = withContext(SmitDispatchers.IO) {
        require(limit > 1)
        val cached = cache.getTimelineBefore(roomId, time, limit)
        val gapIndex = cached.indexOfFirst { it.gapMark != null }

        when {
            gapIndex == -1 -> {
                return@withContext cached
            }

            gapIndex > 0 -> {
                return@withContext cached.take(gapIndex)
            }

            else -> {
                val lastElementBeforeGap = cached.get(gapIndex)
                val elementAfterGap = cached.getOrNull(gapIndex + 1)
                loadTimeline(roomId, lastElementBeforeGap.gapMark!!, elementAfterGap?.gapMark, limit, true)
                getTimelineBefore(roomId, time, limit)
            }
        }
    }

    suspend fun getTimelineAfter(
        roomId: String,
        time: Long,
        limit: Int
    ): List<TimelineEvent> = withContext(SmitDispatchers.IO) {
        require(limit > 1)
        val cached = cache.getTimelineAfter(roomId, time, limit)
        val gapIndex = cached.indexOfFirst { it.gapMark != null }

        when {
            gapIndex == -1 -> {
                return@withContext cached
            }

            gapIndex > 0 -> {
                return@withContext cached.take(gapIndex)
            }

            else -> {
                val lastElementBeforeGap = cached.get(gapIndex)
                val elementAfterGap = cached.getOrNull(gapIndex + 1)
                loadTimeline(roomId, lastElementBeforeGap.gapMark!!, elementAfterGap?.gapMark, limit, false)
                getTimelineBefore(roomId, time, limit)
            }
        }
    }

    suspend fun getMember(memberId: String): Member = withContext(SmitDispatchers.IO) {
        cache.getMember(memberId)?.let { return@withContext it }

        val member = api.getMemberInfo(memberId)
        cache.TRANSACTION { db ->
            db.createMember(member.id)
            db.updateMember(member.id, member.name, member.avatarUrl)
        }

        member
    }

    suspend fun loadJoinedMembersForRoom(roomId: String): List<Member> = withContext(SmitDispatchers.IO) {
        val joined = api.getRoomJoinedMembers(roomId)

        //todo update outdated users

        cache.TRANSACTION { db ->
            joined.forEach { member ->
                db.createMember(member.id)
                db.updateMember(member.id, member.name, member.avatarUrl)
                db.setRoomMember(roomId, member.id, Membership.JOIN)
            }
        }

        joined
    }

    fun convertMediaUrl(rawUrl: String): String = api.convertMediaUrl(rawUrl)

    private suspend fun loadTimeline(roomId: String, from: String, to: String?, limit: Int, isBackward: Boolean) {
        val page = api.loadTimeline(roomId, isBackward, Filter.lazyMembers, from, limit, to)
        cache.TRANSACTION { db ->
            page.chunk.forEach { event ->
                db.createTimelineEvent(
                    event.id,
                    event.type,
                    event.serverTime,
                    roomId,
                    event.sender,
                    event.content,
                    event.stateKey
                )
            }
            db.deleteGapMark(roomId, from)
            if (page.end != null) {
                if (page.end == to) {
                    db.deleteGapMark(roomId, to)
                } else {
                    db.setGapMark(page.chunk.last().id, page.end)
                }
            }
        }
    }
}