package com.github.terrakok.smit.core.entity

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

sealed interface EventContent {
    @Serializable
    class RoomName(
        @SerialName("name") val name: String
    ) : EventContent

    @Serializable
    class RoomTopic(
        @SerialName("topic") val topic: String
    ) : EventContent

    @Serializable
    class RoomMember(
        @SerialName("displayname") val displayName: String? = null,
        @SerialName("avatar_url") val avatarUrl: String? = null,
        @SerialName("membership") val membership: Membership
    ) : EventContent

    @Serializable
    class RoomCreate(
        @SerialName("creator") val creator: String,
        @SerialName("room_version") val version: String,
        @SerialName("type") val type: RoomType? = null
    ) : EventContent

    @Serializable
    class RoomAlias(
        @SerialName("alias") val alias: String? = null,
        @SerialName("alt_aliases") val alternatives: List<String> = emptyList()
    ) : EventContent

    @Serializable
    class RoomAvatar(
        @SerialName("url") val url: String? = null
    ) : EventContent

    @Serializable
    class RoomJoinRules(
        @SerialName("join_rule") val rule: JoinRule
    ) : EventContent

    @Serializable
    class RoomEncryption(
        @SerialName("algorithm") val algorithm: EncryptionAlgorithm,
        @SerialName("rotation_period_ms") val rotationPeriod: Long? = null,
        @SerialName("rotation_period_msgs") val rotationMessages: Long? = null
    ) : EventContent

    @Serializable
    class RoomHistoryVisibility(
        @SerialName("history_visibility") val visibility: HistoryVisibility
    ) : EventContent

    @Serializable
    class RoomGuestAccess(
        @SerialName("guest_access") val access: GuestAccess
    ) : EventContent

    @Serializable
    class RoomMessage(
        @SerialName("msgtype") val type: String? = null,
        @SerialName("body") val body: String? = null
    ) : EventContent

    @Serializable
    class SpaceChild(
        @SerialName("order") val order: String? = null
    ) : EventContent

    @Serializable
    class Unknown(val json: JsonObject) : EventContent
}

object EventContentType {
    const val ROOM_NAME = "m.room.name"
    const val ROOM_TOPIC = "m.room.topic"
    const val ROOM_MEMBER = "m.room.member"
    const val ROOM_CREATE = "m.room.create"
    const val ROOM_CANONICAL_ALIAS = "m.room.canonical_alias"
    const val ROOM_AVATAR = "m.room.avatar"
    const val ROOM_JOIN_RULES = "m.room.join_rules"
    const val ROOM_ENCRYPTION = "m.room.encryption"
    const val ROOM_HISTORY_VISIBILITY = "m.room.history_visibility"
    const val ROOM_MESSAGE = "m.room.message"
    const val ROOM_GUEST_ACCESS = "m.room.guest_access"
    const val ROOM_SPACE_CHILD = "m.space.child"

    internal fun getEventContentSerializer(type: String) = when (type) {
        ROOM_NAME -> EventContent.RoomName.serializer()
        ROOM_TOPIC -> EventContent.RoomTopic.serializer()
        ROOM_MEMBER -> EventContent.RoomMember.serializer()
        ROOM_CREATE -> EventContent.RoomCreate.serializer()
        ROOM_CANONICAL_ALIAS -> EventContent.RoomAlias.serializer()
        ROOM_AVATAR -> EventContent.RoomAvatar.serializer()
        ROOM_JOIN_RULES -> EventContent.RoomJoinRules.serializer()
        ROOM_ENCRYPTION -> EventContent.RoomEncryption.serializer()
        ROOM_HISTORY_VISIBILITY -> EventContent.RoomHistoryVisibility.serializer()
        ROOM_MESSAGE -> EventContent.RoomMessage.serializer()
        ROOM_GUEST_ACCESS -> EventContent.RoomGuestAccess.serializer()
        ROOM_SPACE_CHILD -> EventContent.SpaceChild.serializer()
        else -> null
    }
}