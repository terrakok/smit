package com.github.terrakok.smit.core.gateways

import co.touchlab.kermit.Logger
import com.github.terrakok.smit.core.entity.ErrorResponse
import com.github.terrakok.smit.core.entity.ServerError
import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException

private val logger = Logger.withTag("ErrorResponseHandler")
@Suppress("FunctionName")
internal suspend fun ErrorResponseHandler(exception: Throwable) {
    val clientException = exception as? ClientRequestException ?: return
    val exceptionResponse = clientException.response
    val exceptionResponseData = exceptionResponse.body<ErrorResponse>()
    logger.e(exceptionResponseData.toString())
    throw ServerError(exceptionResponseData)
}