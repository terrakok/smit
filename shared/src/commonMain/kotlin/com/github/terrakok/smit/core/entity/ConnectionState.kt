package com.github.terrakok.smit.core.entity

sealed class ConnectionState {
    object Connected : ConnectionState()
    object Synchronization : ConnectionState()
    data class Connection(val pauseSec: Int = 0, val error: Exception? = null) : ConnectionState()
    data class Disconnected(val error: Exception? = null) : ConnectionState()
}