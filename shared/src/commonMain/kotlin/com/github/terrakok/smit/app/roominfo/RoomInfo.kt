package com.github.terrakok.smit.app.roominfo

import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineBootstrapper
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineExecutorFactory
import com.github.terrakok.smit.core.SmitService
import com.github.terrakok.smit.core.entity.AppEvent
import com.github.terrakok.smit.core.entity.Member
import com.github.terrakok.smit.core.entity.Room
import com.github.terrakok.smit.core.entity.RoomType
import com.github.terrakok.smit.core.entity.TimelineEvent
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

object RoomInfo {
    data class State(
        val room: Room? = null,
        val heroes: List<Member> = emptyList(),
        val lastEvent: TimelineEvent? = null
    ) {
        val title: String
            get() {
                if (room == null) return ""
                val title = room.name ?: room.aliases.firstOrNull()
                return title ?: if (heroes.isNotEmpty()) {
                    heroes.joinToString { it.name ?: it.id }
                } else ""
            }

        val subtitle: String
            get() {
                return if (room?.type == null) lastEvent?.type.orEmpty()
                else ""
            }

        val symbols: String
            get() = when (room?.type) {
                RoomType.SPACE -> "#"
                else -> title.take(2).uppercase()
            }

        val avatarUrl: String?
            get() = when {
                room?.avatarUrl != null -> room.avatarUrl
                heroes.size == 1 -> heroes.first().avatarUrl
                else -> null
            }
    }

    sealed interface Intent
    sealed interface Label
    private sealed interface Action {
        data class Loaded(val room: Room?) : Action
    }

    private sealed interface Msg {
        data class RoomLoaded(val room: Room?) : Msg
        data class HeroesLoaded(val heroes: List<Member>) : Msg
        data class LastEventLoaded(val event: TimelineEvent) : Msg
    }

    fun createStore(roomId: String, factory: StoreFactory, smitService: SmitService): Store<Intent, State, Label> =
        factory.create<Intent, Action, Msg, State, Label>(
            name = "RoomInfoStore[$roomId]",
            initialState = State(),
            bootstrapper = coroutineBootstrapper {
                launch {
                    smitService.events.filterIsInstance<AppEvent.RoomStateChanged>()
                        .filter { it.id == roomId }
                        .map { smitService.getRoom(roomId) }
                        .onStart { emit(smitService.getRoom(roomId)) }
                        .collect { dispatch(Action.Loaded(it)) }
                }
            },
            executorFactory = coroutineExecutorFactory {
                onAction<Action.Loaded> {
                    launch {
                        dispatch(Msg.RoomLoaded(it.room))
                        val room = it.room ?: return@launch

                        launch {
                            val members = room.heroes.map {
                                async { smitService.getMember(it) }
                            }.awaitAll().filterNotNull()
                            dispatch(Msg.HeroesLoaded(members))
                        }
                        launch {
                            val event = smitService.getLastTimelineEvent(roomId)
                            if (event != null) {
                                dispatch(Msg.LastEventLoaded(event))
                            }
                        }
                    }
                }
            },
            reducer = { msg ->
                when (msg) {
                    is Msg.RoomLoaded -> copy(room = msg.room)
                    is Msg.HeroesLoaded -> copy(heroes = msg.heroes)
                    is Msg.LastEventLoaded -> copy(lastEvent = msg.event)
                }
            }
        )
}