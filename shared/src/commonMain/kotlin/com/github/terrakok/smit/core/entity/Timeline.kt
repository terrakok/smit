package com.github.terrakok.smit.core.entity

sealed interface TimelineItem

class TimelineEvent(
    val id: String,
    val type: String,
    val time: Long,
    val roomId: String,
    val senderId: String,
    val gapMark: String?,
    val stateKey: String,
    val content: EventContent
) : TimelineItem

class MessagesPack(
    val events: List<TimelineEvent>,
    val senderId: String
) : TimelineItem

class MembersPack(
    val events: List<TimelineEvent>
) : TimelineItem