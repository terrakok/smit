package com.github.terrakok.smit.core.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Filter(
    @SerialName("room") val roomFilter: RoomFilter
) {
    companion object {
        val lazyMembers = Filter(
            RoomFilter(
                StateFilter(lazyLoadMembers = true),
                RoomEventFilter(lazyLoadMembers = true)
            )
        )
    }
}

@Serializable
class RoomFilter(
    @SerialName("state") val stateFilter: StateFilter,
    @SerialName("timeline") val timelineFilter: RoomEventFilter
)

@Serializable
class RoomEventFilter(
    @SerialName("lazy_load_members") val lazyLoadMembers: Boolean
)

@Serializable
class StateFilter(
    @SerialName("lazy_load_members") val lazyLoadMembers: Boolean
)