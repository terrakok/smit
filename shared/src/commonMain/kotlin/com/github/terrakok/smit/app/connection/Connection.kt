package com.github.terrakok.smit.app.connection

import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineBootstrapper
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineExecutorFactory
import com.github.terrakok.smit.core.SmitService
import com.github.terrakok.smit.core.entity.ConnectionState
import kotlinx.coroutines.launch

object Connection {
    data class State(
        val state: ConnectionState = ConnectionState.Disconnected()
    )

    sealed interface Intent {
        object Connect : Intent
        object Disconnect : Intent
    }

    sealed interface Label
    private sealed interface Action {
        data class NewState(val state: ConnectionState) : Action
    }

    private sealed interface Msg {
        data class NewState(val state: ConnectionState) : Msg
    }

    fun createStore(factory: StoreFactory, smitService: SmitService): Store<Intent, State, Label> =
        factory.create<Intent, Action, Msg, State, Label>(
            name = "ConnectionStore",
            initialState = State(),
            bootstrapper = coroutineBootstrapper {
                launch {
                    smitService.connectionState.collect { state ->
                        dispatch(Action.NewState(state))
                    }
                }
            },
            executorFactory = coroutineExecutorFactory {
                onAction<Action.NewState> {
                    dispatch(Msg.NewState(it.state))
                }
                onIntent<Intent.Connect> {
                    smitService.connect()
                }
                onIntent<Intent.Disconnect> {
                    smitService.disconnect()
                }
            },
            reducer = { msg ->
                when (msg) {
                    is Msg.NewState -> State(msg.state)
                }
            }
        )
}