package com.github.terrakok.smit.core.entity

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.EmptyCoroutineContext

internal val SmitGlobalScope = CoroutineScope(EmptyCoroutineContext)

internal expect object SmitDispatchers {
    val IO: CoroutineDispatcher
}