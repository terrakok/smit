package com.github.terrakok.smit.app.rooms

import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineBootstrapper
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineExecutorFactory
import com.github.terrakok.smit.core.SmitService
import com.github.terrakok.smit.core.entity.AppEvent
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

object RoomList {
    const val DIRECT_ROOMS_ID = ""
    val ROOT_ROOM_ID: String? = null

    data class State(
        val roomIds: List<String> = emptyList()
    )

    sealed interface Intent
    sealed interface Label

    private sealed interface Action {
        data class Loaded(val ids: List<String>) : Action
    }

    private sealed interface Msg {
        data class NewState(val ids: List<String>) : Msg
    }

    fun createStore(id: String?, factory: StoreFactory, smitService: SmitService): Store<Intent, State, Label> =
        factory.create<Intent, Action, Msg, State, Label>(
            name = "RoomListStore($id)",
            initialState = State(),
            bootstrapper = coroutineBootstrapper {
                launch {
                    suspend fun request() = when (id) {
                        DIRECT_ROOMS_ID -> smitService.getDirectRoomIds()
                        ROOT_ROOM_ID -> smitService.getSpaceRoomIds(null)
                        else -> smitService.getSpaceRoomIds(id)
                    }
                    smitService.events
                        .filter { it is AppEvent.RoomStateChanged }
                        .debounce(200L)
                        .map { request() }
                        .onStart { emit(request()) }
                        .collect { dispatch(Action.Loaded(it)) }
                }
            },
            executorFactory = coroutineExecutorFactory {
                onAction<Action.Loaded> {
                    dispatch(Msg.NewState(it.ids))
                }
            },
            reducer = { msg ->
                when (msg) {
                    is Msg.NewState -> State(msg.ids)
                }
            }
        )
}