package com.github.terrakok.smit.core.domain

import co.touchlab.kermit.Logger
import com.github.terrakok.smit.core.entity.AccountEvent
import com.github.terrakok.smit.core.entity.AppEvent
import com.github.terrakok.smit.core.entity.ClientEvent
import com.github.terrakok.smit.core.entity.EventContent
import com.github.terrakok.smit.core.entity.EventContentType
import com.github.terrakok.smit.core.entity.JoinedRoom
import com.github.terrakok.smit.core.entity.LeftRoom
import com.github.terrakok.smit.core.entity.Membership
import com.github.terrakok.smit.core.entity.SyncResponse
import com.github.terrakok.smit.core.entity.Timeline
import com.github.terrakok.smit.core.gateways.Cache
import com.github.terrakok.smit.core.gateways.Storage
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonPrimitive

internal class ServerEventHandler(
    private val cache: Cache,
    private val eventCollector: EventCollector,
    private val storage: Storage,
    private val json: Json
) {
    private val log = Logger.withTag("ServerEventHandler")

    fun handleServerEvent(since: String?, event: SyncResponse) {
        event.accountData?.let { accountData ->
            handleAccountEvents(accountData.events)
        }
        event.rooms?.join?.let { rooms ->
            cache.TRANSACTION { db -> handleJoinedRooms(db, rooms, since) }
        }
        event.rooms?.leave?.let { rooms ->
            cache.TRANSACTION { db -> handleLeftRooms(db, rooms) }
        }
    }

    private fun handleAccountEvents(events: List<AccountEvent>) {
        events.forEach { event ->
            log.d("New account event: ${event.type}")
            when (event.type) {
                "m.direct" -> {
                    //https://spec.matrix.org/v1.4/client-server-api/#mdirect
                    val directRooms = event.content.entries.associate { (memberId, arr) ->
                        memberId to arr.jsonArray.map { it.jsonPrimitive.content }
                    }
                    storage.directRooms = directRooms
                }

                else -> {
                    log.d("handleAccountEvents skipped: ${event.type}")
                }
            }
        }
    }

    private fun handleJoinedRooms(db: Cache, rooms: Map<String, JoinedRoom>, since: String?) {
        rooms.forEach { (id, room) ->
            db.createRoom(id)
            val roomEvents = room.state.events.sortedBy { it.serverTime } +
                    room.timeline.events.sortedBy { it.serverTime }
            roomEvents.forEach { event -> handleRoomEvent(db, id, event) }
            roomEvents.lastOrNull()?.let { lastEvent ->
                db.updateRoom(id, updated = lastEvent.serverTime)
            }

            val heroes = room.summary.heroes
            if (heroes != null) {
                db.updateRoom(id, heroes = heroes)
            }
            updateTimeline(db, id, since, room.timeline)

            eventCollector.newEvent(AppEvent.RoomStateChanged(id))
        }
    }

    private fun handleLeftRooms(db: Cache, rooms: Map<String, LeftRoom>) {
        rooms.keys.forEach { id ->
            db.deleteRoom(id)
            eventCollector.newEvent(AppEvent.RoomWasDeleted(id))
        }
    }

    private fun handleRoomEvent(db: Cache, id: String, event: ClientEvent) {
        log.d("New room($id) event: ${event.type}")
        when (event.type) {
            EventContentType.ROOM_CANONICAL_ALIAS -> {
                val content: EventContent.RoomAlias = json.decodeFromJsonElement(event.content)
                val aliases = listOfNotNull(
                    content.alias,
                    *content.alternatives.toTypedArray()
                )
                db.updateRoom(id, aliases = aliases)
            }

            EventContentType.ROOM_CREATE -> {
                val content: EventContent.RoomCreate = json.decodeFromJsonElement(event.content)
                db.updateRoom(id, type = content.type, creator = content.creator, version = content.version)
            }

            EventContentType.ROOM_NAME -> {
                val content: EventContent.RoomName = json.decodeFromJsonElement(event.content)
                db.updateRoom(id, name = content.name)
            }

            EventContentType.ROOM_TOPIC -> {
                val content: EventContent.RoomTopic = json.decodeFromJsonElement(event.content)
                val topic = content.topic
                if (topic.isBlank()) {
                    db.deleteRoomTopic(id)
                } else {
                    db.updateRoom(id, topic = topic)
                }
            }

            EventContentType.ROOM_AVATAR -> {
                val content: EventContent.RoomAvatar = json.decodeFromJsonElement(event.content)
                if (content.url != null) {
                    db.updateRoom(id, avatarUrl = content.url)
                } else {
                    db.deleteRoomAvatar(id)
                }
            }

            EventContentType.ROOM_ENCRYPTION -> {
                val content: EventContent.RoomEncryption = json.decodeFromJsonElement(event.content)
                db.updateRoom(id, encryption = content.algorithm)
            }

            EventContentType.ROOM_JOIN_RULES -> {
                val content: EventContent.RoomJoinRules = json.decodeFromJsonElement(event.content)
                db.updateRoom(id, joinRule = content.rule)
            }

            EventContentType.ROOM_HISTORY_VISIBILITY -> {
                val content: EventContent.RoomHistoryVisibility = json.decodeFromJsonElement(event.content)
                db.updateRoom(id, historyVisibility = content.visibility)
            }

            EventContentType.ROOM_GUEST_ACCESS -> {
                val content: EventContent.RoomGuestAccess = json.decodeFromJsonElement(event.content)
                db.updateRoom(id, guestAccess = content.access)
            }

            EventContentType.ROOM_MEMBER -> {
                val content: EventContent.RoomMember = json.decodeFromJsonElement(event.content)
                val memberId = event.stateKey
                db.createMember(memberId)
                db.updateMember(memberId, content.displayName, content.avatarUrl)
                db.setRoomMember(id, memberId, content.membership)
            }

            EventContentType.ROOM_SPACE_CHILD -> {
                val content: EventContent.SpaceChild = json.decodeFromJsonElement(event.content)
                val childId = event.stateKey
                db.addChildToRoom(id, childId)
            }

            else -> {
                log.d("handleRoomEvents skipped: ${event.type}")
            }
        }
    }

    private fun updateTimeline(
        db: Cache,
        roomId: String,
        since: String?,
        timeline: Timeline
    ) {
        log.d("updateTimeline($roomId) ${timeline.events.size} events. limited=${timeline.limited}")
        if (timeline.events.isEmpty()) return

        //sorted OLD -> NEW
        val orderedEvents = timeline.events.sortedBy { it.serverTime }

        val lastCachedEvenId = db.getLastEventId(roomId)
        val firstNewEvenId = orderedEvents.first().id

        orderedEvents.forEach { event ->
            db.createTimelineEvent(event.id, event.type, event.serverTime, roomId, event.sender, event.content, event.stateKey)
        }

        if (timeline.limited) {
            if (lastCachedEvenId != null) {
                db.setGapMark(lastCachedEvenId, since)
            }
            db.setGapMark(firstNewEvenId, timeline.previousBatch)
        } else {
            if (lastCachedEvenId != null) {
                db.setGapMark(lastCachedEvenId, null)
            }
        }

        eventCollector.newEvent(AppEvent.RoomNewMessages(roomId, timeline.limited))
    }
}