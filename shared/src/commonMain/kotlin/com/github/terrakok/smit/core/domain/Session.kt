package com.github.terrakok.smit.core.domain

import co.touchlab.kermit.Logger
import com.github.terrakok.smit.core.entity.ConnectionState
import com.github.terrakok.smit.core.entity.SmitGlobalScope
import com.github.terrakok.smit.core.entity.UserSession
import com.github.terrakok.smit.core.gateways.Cache
import com.github.terrakok.smit.core.gateways.MatrixApi
import com.github.terrakok.smit.core.gateways.Storage
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

internal class Session(
    private val api: MatrixApi,
    private val storage: Storage,
    private val connection: Connection,
    private val cache: Cache
) {
    private val log = Logger.withTag("Session")
    val connectionState: StateFlow<ConnectionState> = connection.connectionState

    private val currentUserSession: UserSession? get() = storage.currentUserSession
    private val _sessionState = MutableStateFlow(currentUserSession != null)
    val sessionState: StateFlow<Boolean> = _sessionState.asStateFlow()
    val currentUserId: String? get() = currentUserSession?.userId

    suspend fun login(host: String, user: String, password: String): String {
        log.d("login")
        if (currentUserSession != null) error("You must logout before new login!")

        val session = api.login(host, user, password, storage.deviceId)
        storage.currentUserSession = session
        storage.deviceId = session.deviceId
        _sessionState.emit(true)
        return session.userId
    }

    fun logout() {
        log.d("logout")
        disconnect()

        SmitGlobalScope.launch {
            _sessionState.emit(false)
            try {
                api.logout()
            } catch (e: Throwable) {
                //do nothing
            }
            cache.clear()
            val deviceId = storage.deviceId
            storage.clear()
            storage.deviceId = deviceId
        }
    }

    fun connect() {
        log.d("connect")
        connection.connect()
    }

    fun disconnect() {
        log.d("disconnect")
        connection.disconnect()
    }
}
