package com.github.terrakok.smit.core

import com.github.terrakok.smit.core.entity.ConnectionState
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.job
import kotlinx.coroutines.runBlocking
import kotlin.test.Test

private const val LOGIN = ""
private const val PASSWORD = ""

expect fun createPlatformSpecific(): PlatformSpecific

class ServiceTest {
    private val smit = SmitService.create(createPlatformSpecific(), true)

    @Test
    fun getRooms(): Unit = runBlocking {
        if (smit.hasSession()) smit.logout()
        smit.login(LOGIN, PASSWORD)

        smit.connect()
        smit.connectionState.first { it is ConnectionState.Connected }

        println("ROOMS = ${smit.getRooms()}")

        smit.logout()
        SmitGlobalScope.coroutineContext.job.children.forEach { it.join() }
    }
}