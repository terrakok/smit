package com.github.terrakok.smit.core

import com.github.terrakok.smit.core.db.SmitDB
import com.russhwolf.settings.JvmPreferencesSettings
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp

class JvmPlatform : PlatformSpecific {
    override val deviceName = "JVM device"
    override fun createHttpClient(): HttpClient = HttpClient(OkHttp)
    override fun createSqlDriver(): JdbcSqliteDriver = JdbcSqliteDriver(JdbcSqliteDriver.IN_MEMORY).also {
        SmitDB.Schema.create(it)
    }
    override fun createSettings() = JvmPreferencesSettings.Factory().create("SmitPref")
}
