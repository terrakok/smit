package com.github.terrakok.smit.core

import com.github.terrakok.smit.core.db.SmitDB
import com.russhwolf.settings.JvmPropertiesSettings
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import java.util.Properties

actual fun createPlatformSpecific(): PlatformSpecific = object : PlatformSpecific {
    override val deviceName = "Unit test"

    override fun createHttpClient() = HttpClient(OkHttp)

    override fun createSqlDriver() = JdbcSqliteDriver(JdbcSqliteDriver.IN_MEMORY).also {
        SmitDB.Schema.create(it)
    }

    override fun createSettings() = JvmPropertiesSettings(Properties())
}