package com.github.terrakok.smit.core.entity

import kotlinx.coroutines.Dispatchers

internal actual object SmitDispatchers {
    actual val IO = Dispatchers.Default
}