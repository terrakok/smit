package com.github.terrakok.smit.core

import com.github.terrakok.smit.core.db.SmitDB
import com.russhwolf.settings.AppleSettings
import com.russhwolf.settings.Settings
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver
import io.ktor.client.HttpClient
import io.ktor.client.engine.darwin.Darwin

class IosPlatform : PlatformSpecific {
    override val deviceName: String = "iOS device"
    override fun createHttpClient(): HttpClient = HttpClient(Darwin) {
        engine {
            configureRequest {
                setAllowsCellularAccess(true)
            }
        }
    }
    override fun createSqlDriver(): SqlDriver = NativeSqliteDriver(SmitDB.Schema, "Smit.db")
    override fun createSettings(): Settings = AppleSettings.Factory().create("SmitPref")
}