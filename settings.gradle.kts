pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
    plugins {
        val kotlin = "1.7.20"
        kotlin("android").version(kotlin)
        kotlin("multiplatform").version(kotlin)
        kotlin("plugin.serialization").version(kotlin)
        val agp = "7.3.1"
        id("com.android.application").version(agp)
        id("com.android.library").version(agp)
        id("com.squareup.sqldelight").version("1.5.4")
        id("com.github.ben-manes.versions").version("0.44.0")
    }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
    }
    versionCatalogs {
        create("libs") {
            val composeUI = "1.3.2"
            library("compose-ui", "androidx.compose.ui:ui:$composeUI")
            library("compose-ui-tooling", "androidx.compose.ui:ui-tooling:$composeUI")
            library("compose-ui-tooling-preview", "androidx.compose.ui:ui-tooling-preview:$composeUI")
            val compose = "1.3.1"
            library("compose-foundation", "androidx.compose.foundation:foundation:$compose")
            library("compose-material", "androidx.compose.material:material:$compose")
            library("compose-material-icons", "androidx.compose.material:material-icons-extended:$compose")

            library("activity-compose", "androidx.activity:activity-compose:1.6.1")

            val ktor = "2.2.2"
            library("ktor-client-core", "io.ktor:ktor-client-core:$ktor")
            library("ktor-client-content-negotiation", "io.ktor:ktor-client-content-negotiation:$ktor")
            library("ktor-client-logging", "io.ktor:ktor-client-logging:$ktor")
            library("ktor-client-auth", "io.ktor:ktor-client-auth:$ktor")
            library("ktor-serialization-kotlinx-json", "io.ktor:ktor-serialization-kotlinx-json:$ktor")
            library("ktor-client-okhttp", "io.ktor:ktor-client-okhttp:$ktor")
            library("ktor-client-darwin", "io.ktor:ktor-client-darwin:$ktor")

            val coroutines = "1.6.4"
            library("kotlinx-coroutines-core", "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines")
            library("kotlinx-coroutines-android", "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutines")

            library("kotlinx-serialization-json", "org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")

            val sqldelight = "1.5.4"
            library("sqldelight-jvm-driver", "com.squareup.sqldelight:sqlite-driver:$sqldelight")
            library("sqldelight-android-driver", "com.squareup.sqldelight:android-driver:$sqldelight")
            library("sqldelight-native-driver", "com.squareup.sqldelight:native-driver:$sqldelight")

            library("kermit", "co.touchlab:kermit:1.2.2")
            library("multiplatform-settings", "com.russhwolf:multiplatform-settings:0.9")
            library("koin-core", "io.insert-koin:koin-core:3.3.2")
            library("koin-androidx-compose", "io.insert-koin:koin-androidx-compose:3.4.1")

            val voyager = "1.0.0-rc03"
            library("voyager-navigator", "cafe.adriel.voyager:voyager-navigator:$voyager")
            library("voyager-navigator-tab", "cafe.adriel.voyager:voyager-tab-navigator:$voyager")

            val mvikotlin = "3.0.2"
            library("mvikotlin-core", "com.arkivanov.mvikotlin:mvikotlin:$mvikotlin")
            library("mvikotlin-main", "com.arkivanov.mvikotlin:mvikotlin-main:$mvikotlin")
            library("mvikotlin-logging", "com.arkivanov.mvikotlin:mvikotlin-logging:$mvikotlin")
            library("mvikotlin-coroutines", "com.arkivanov.mvikotlin:mvikotlin-extensions-coroutines:$mvikotlin")

            library("coil-compose", "io.coil-kt:coil-compose:2.2.2")
        }
    }
}

rootProject.name = "Smit"
include(":androidApp")
include(":shared")