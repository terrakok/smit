plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    namespace = "com.github.terrakok.smit.android"
    compileSdk = (findProperty("android.sdk.target") as String).toInt()
    defaultConfig {
        applicationId = "com.github.terrakok.smit.android"
        minSdk = (findProperty("android.sdk.min") as String).toInt()
        targetSdk = (findProperty("android.sdk.target") as String).toInt()
        versionCode = 1
        versionName = "1.0"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = findProperty("android.compose.compiler") as String
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
        create("debugPG") {
            matchingFallbacks.add("debug")
            signingConfig = signingConfigs["debug"]
            isDebuggable = true
            isMinifyEnabled = true
            proguardFile("proguard-rules.pro")
        }
    }
    kotlinOptions {
        freeCompilerArgs = listOf(
            "-opt-in=androidx.compose.ui.ExperimentalComposeUiApi",
            "-opt-in=androidx.compose.ui.text.ExperimentalTextApi"
        )
    }
}

dependencies {
    implementation(project(":shared"))
    implementation(libs.compose.ui)
    implementation(libs.compose.ui.tooling)
    implementation(libs.compose.ui.tooling.preview)
    implementation(libs.compose.foundation)
    implementation(libs.compose.material)
    implementation(libs.compose.material.icons)
    implementation(libs.activity.compose)
    implementation(libs.koin.core)
    implementation(libs.koin.androidx.compose)
    implementation(libs.voyager.navigator)
    implementation(libs.voyager.navigator.tab)
    implementation(libs.mvikotlin.core)
    implementation(libs.mvikotlin.main)
    implementation(libs.mvikotlin.logging)
    implementation(libs.mvikotlin.coroutines)
    implementation(libs.coil.compose)
}