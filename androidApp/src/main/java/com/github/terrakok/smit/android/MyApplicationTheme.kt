package com.github.terrakok.smit.android

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Shapes
import androidx.compose.material.Typography
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun MyApplicationTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        darkColors(
            primary = Color(0xFF457b9d),
            primaryVariant = Color(0xFF1d3557),
            secondary = Color(0xFFa8dadc),
            secondaryVariant = Color(0xFFa8dadc),
            background = Color(0xFF121212),
            surface = Color(0xFF000000),
            error = Color(0xFFe63946),
            onPrimary = Color(0xFFf1faee),
            onSecondary = Color(0xFFf1faee),
            onBackground = Color(0xFFf1faee),
            onSurface = Color(0xFFf1faee),
            onError = Color(0xFFFFFFFF)
        )
    } else {
        lightColors(
            primary = Color(0xFF457b9d),
            primaryVariant = Color(0xFF1d3557),
            secondary = Color(0xFFa8dadc),
            secondaryVariant = Color(0xFFa8dadc),
            background = Color(0xFFFFFFFF),
            surface = Color(0xFFf8f8f8),
            error = Color(0xFFe63946),
            onPrimary = Color(0xFFf1faee),
            onSecondary = Color(0xFFf1faee),
            onBackground = Color(0xFF121212),
            onSurface = Color(0xFF121212),
            onError = Color(0xFFFFFFFF)
        )
    }
    val typography = Typography(
        body1 = TextStyle(
            fontFamily = FontFamily.Default,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp
        )
    )
    val shapes = Shapes(
        small = RoundedCornerShape(4.dp),
        medium = RoundedCornerShape(8.dp),
        large = RoundedCornerShape(16.dp)
    )

    MaterialTheme(
        colors = colors,
        typography = typography,
        shapes = shapes,
        content = content
    )
}
