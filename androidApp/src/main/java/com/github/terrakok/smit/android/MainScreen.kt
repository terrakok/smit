package com.github.terrakok.smit.android

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.rememberScreenModel
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.navigator.tab.CurrentTab
import cafe.adriel.voyager.navigator.tab.LocalTabNavigator
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabNavigator
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.extensions.coroutines.states
import com.github.terrakok.smit.app.connection.Connection
import com.github.terrakok.smit.core.entity.ConnectionState
import kotlinx.coroutines.delay
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.qualifier.named

class MainScreen : Screen {
    override val key = uniqueScreenKey

    private class ScreenScope : ScreenModel, KoinComponent {
        val connectionStore by inject<Store<Connection.Intent, Connection.State, Connection.Label>>(named(StoreName.Connection))
    }

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @Composable
    override fun Content() {
        val scope = rememberScreenModel { ScreenScope() }
        val connectionState by scope.connectionStore.states.collectAsState(initial = Connection.State())

        var connectionPause by rememberSaveable { mutableStateOf(0) }
        LaunchedEffect(key1 = connectionState.state) {
            val currentState = connectionState.state
            if (currentState is ConnectionState.Connection) {
                connectionPause = currentState.pauseSec
            }
            while (connectionPause > 0 && connectionState.state is ConnectionState.Connection) {
                delay(1000)
                connectionPause--
            }
        }

        LaunchedEffect(Unit) {
            scope.connectionStore.accept(Connection.Intent.Connect)
        }

        TabNavigator(HomeScreen) {
            Scaffold(
                content = { CurrentTab() },
                bottomBar = {
                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .background(MaterialTheme.colors.primary)) {
                        val subtitleText = when (connectionState.state) {
                            is ConnectionState.Connection -> {
                                if (connectionPause > 1) "Connection in $connectionPause sec"
                                else "Connection..."
                            }

                            is ConnectionState.Synchronization -> "Synchronization..."
                            else -> null
                        }
                        if (subtitleText != null) {
                            Text(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .clickable { scope.connectionStore.accept(Connection.Intent.Connect) }
                                    .padding(4.dp),
                                text = subtitleText,
                                textAlign = TextAlign.Center,
                                fontSize = 10.sp,
                                color = MaterialTheme.colors.onPrimary
                            )
                        }
                        BottomNavigation {
                            TabNavigationItem(HomeScreen)
                            TabNavigationItem(DirectRoomsScreen)
                            TabNavigationItem(ProfileScreen)
                        }
                    }
                }
            )
        }
    }
}

@Composable
private fun RowScope.TabNavigationItem(tab: Tab) {
    val tabNavigator = LocalTabNavigator.current
    BottomNavigationItem(
        selected = tabNavigator.current == tab,
        onClick = { tabNavigator.current = tab },
        icon = { Icon(painter = tab.options.icon!!, contentDescription = tab.options.title) }
    )
}