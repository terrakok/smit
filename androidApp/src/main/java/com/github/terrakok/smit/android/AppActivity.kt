package com.github.terrakok.smit.android

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.Navigator
import com.github.terrakok.smit.core.SmitService
import org.koin.androidx.compose.get

class AppActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val smitService = get<SmitService>()
            MyApplicationTheme {
                Surface(
                    modifier = Modifier
                        .fillMaxSize()
                        .systemBarsPadding(),
                    color = MaterialTheme.colors.background
                ) {
                    val startScreen = if (smitService.sessionState.value) MainScreen() else LoginScreen()
                    Navigator(startScreen) {
                        LaunchedEffect(Unit) {
                            smitService.sessionState.collect { hasSession ->
                                if (!hasSession) {
                                    it.replaceAll(LoginScreen())
                                }
                            }
                        }
                        CurrentScreen()
                    }
                }
            }
        }
    }
}

