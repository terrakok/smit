package com.github.terrakok.smit.android

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.People
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.LineHeightStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.rememberScreenModel
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import coil.compose.AsyncImage
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.extensions.coroutines.states
import com.github.terrakok.smit.app.roominfo.RoomInfo
import com.github.terrakok.smit.app.rooms.RoomList
import com.github.terrakok.smit.core.entity.RoomType
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.component.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named

data class RoomListScreen(private val id: String?) : Screen {
    override val key = uniqueScreenKey

    class ScreenScope(private val id: String?) : ScreenModel, KoinComponent {
        val roomListStore by inject<Store<RoomList.Intent, RoomList.State, RoomList.Label>>(
            named(StoreName.RoomList),
            parameters = { parametersOf(id) }
        )
        private val roomInfoStores = mutableMapOf<String, Store<RoomInfo.Intent, RoomInfo.State, RoomInfo.Label>>()
        fun roomInfo(id: String) = roomInfoStores.getOrPut(id) {
            get(named(StoreName.RoomInfo), parameters = { parametersOf(id) })
        }
    }

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @Composable
    override fun Content() {
        val scope = rememberScreenModel { ScreenScope(id) }
        val roomListState by scope.roomListStore.states.collectAsState(initial = scope.roomListStore.state)

        Scaffold(
            topBar = {
                when (id) {
                    RoomList.DIRECT_ROOMS_ID -> TopAppBar(title = { Text(text = "Direct messages") })
                    RoomList.ROOT_ROOM_ID -> TopAppBar(title = { Text(text = "Home") })
                    else -> RoomInfoAppBar(scope.roomInfo(id!!))
                }
            },
            content = {
                LazyColumn {
                    items(
                        items = roomListState.roomIds,
                        key = { it }
                    ) {
                        Column {
                            RoomItem(scope.roomInfo(it))
                            Divider(modifier = Modifier.padding(start = 74.dp))
                        }
                    }
                }
            }
        )
    }
}

@Composable
internal fun RoomInfoAppBar(
    roomInfoStore: Store<RoomInfo.Intent, RoomInfo.State, RoomInfo.Label>
) {
    val navigator = LocalNavigator.currentOrThrow
    val roomInfoState by roomInfoStore.states.collectAsState(initial = roomInfoStore.state)

    TopAppBar(
        navigationIcon = {
            Icon(
                modifier = Modifier
                    .clickable { navigator.pop() }
                    .padding(8.dp),
                imageVector = Icons.Filled.ArrowBack,
                contentDescription = null
            )
        },
        title = { Text(text = roomInfoState.title) }
    )
}

@Composable
private fun RoomItem(roomInfoStore: Store<RoomInfo.Intent, RoomInfo.State, RoomInfo.Label>) {
    val roomState by roomInfoStore.states.collectAsState(initial = roomInfoStore.state)
    val room = roomState.room
    if (room != null) {
        val navigator = LocalNavigator.currentOrThrow
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    when (room.type) {
                        RoomType.SPACE -> {
                            navigator.push(RoomListScreen(room.id))
                        }

                        else -> {
                            //fixme
                            navigator.parent?.parent?.push(RoomScreen(room.id))
                        }
                    }
                }
                .padding(start = 8.dp, end = 8.dp)
        ) {

            val subtitle = when (room.type) {
                RoomType.SPACE -> room.topic
                else -> roomState.subtitle
            }

            RoomAvatar(
                roomState = roomState,
                modifier = Modifier.align(Alignment.CenterVertically)
            )

            Column(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(start = 8.dp)
            ) {
                Text(
                    text = roomState.title,
                    style = MaterialTheme.typography.subtitle1,
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
                if (subtitle != null) {
                    Text(
                        text = subtitle,
                        style = MaterialTheme.typography.caption,
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 1
                    )
                }
            }
        }
    } else {
        Spacer(modifier = Modifier.height(60.dp))
    }
}

@Composable
private fun RoomAvatar(roomState: RoomInfo.State, modifier: Modifier) {
    val bgShape = when (roomState.room?.type) {
        RoomType.SPACE -> RoundedCornerShape(percent = 20)
        else -> CircleShape
    }
    Box(
        modifier = modifier
            .width(60.dp)
            .height(60.dp)
            .padding(8.dp)
            .background(MaterialTheme.colors.secondary, bgShape)
    ) {
        val avatarUrl = roomState.avatarUrl
        if (avatarUrl == null) {
            if (roomState.heroes.size > 1) {
                Image(
                    modifier = Modifier
                        .align(Alignment.Center)
                        .padding(2.dp)
                        .clip(bgShape),
                    colorFilter = ColorFilter.tint(MaterialTheme.colors.onSecondary),
                    imageVector = Icons.Filled.People,
                    contentDescription = null
                )
            } else {
                Text(
                    modifier = Modifier.align(Alignment.Center),
                    color = MaterialTheme.colors.onSecondary,
                    fontSize = 20.sp,
                    style = LocalTextStyle.current.merge(
                        @Suppress("DEPRECATION")
                        TextStyle(
                            platformStyle = PlatformTextStyle(
                                includeFontPadding = false
                            ),
                            lineHeightStyle = LineHeightStyle(
                                alignment = LineHeightStyle.Alignment.Center,
                                trim = LineHeightStyle.Trim.None
                            )
                        )
                    ),
                    text = roomState.symbols
                )
            }
        } else {
            AsyncImage(
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(2.dp)
                    .clip(bgShape),
                model = App.convertMediaUrl(avatarUrl),
                contentDescription = null
            )
        }
    }
}