package com.github.terrakok.smit.android

import android.app.Application
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.logging.store.LoggingStoreFactory
import com.arkivanov.mvikotlin.main.store.DefaultStoreFactory
import com.github.terrakok.smit.app.connection.Connection
import com.github.terrakok.smit.app.login.Login
import com.github.terrakok.smit.app.roominfo.RoomInfo
import com.github.terrakok.smit.app.rooms.RoomList
import com.github.terrakok.smit.app.timeline.Timeline
import com.github.terrakok.smit.core.AndroidPlatform
import com.github.terrakok.smit.core.SmitService
import com.github.terrakok.smit.core.create
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module

object StoreName {
    const val Login = "Login"
    const val Connection = "Connection"
    const val RoomList = "RoomList"
    const val RoomInfo = "RoomInfo"
    const val Timeline = "Timeline"
}

class App : Application() {

    private val appModule = module {
        single { SmitService.create(AndroidPlatform(this@App), BuildConfig.DEBUG) }
        single<StoreFactory> { DefaultStoreFactory() }
        factory(named(StoreName.Login)) { Login.createStore(get(), get()) }
        factory(named(StoreName.Connection)) { Connection.createStore(get(), get()) }
        factory(named(StoreName.RoomList)) { (id: String?) -> RoomList.createStore(id, get(), get()) }
        factory(named(StoreName.RoomInfo)) { (id: String) -> RoomInfo.createStore(id, get(), get()) }
        factory(named(StoreName.Timeline)) { (id: String) -> Timeline.createStore(id, get(), get()) }
    }

    override fun onCreate() {
        super.onCreate()
        val koinApp = startKoin { modules(appModule) }

        smitService = koinApp.koin.get()
    }

    companion object {
        private lateinit var smitService: SmitService
        val currentUserId get() = smitService.currentUserId
        fun convertMediaUrl(rawUrl: String): String = smitService.convertMediaUrl(rawUrl)
    }
}