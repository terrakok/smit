package com.github.terrakok.smit.android

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.LineHeightStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.rememberScreenModel
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import coil.compose.AsyncImage
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.extensions.coroutines.states
import com.github.terrakok.smit.app.roominfo.RoomInfo
import com.github.terrakok.smit.app.timeline.Timeline
import com.github.terrakok.smit.core.entity.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named

data class RoomScreen(val id: String) : Screen {
    override val key = uniqueScreenKey

    class ScreenScope(private val id: String) : ScreenModel, KoinComponent {
        val currentUserId = App.currentUserId.orEmpty()
        val timelineStore by inject<Store<Timeline.Intent, Timeline.State, Timeline.Label>>(
            named(StoreName.Timeline),
            parameters = { parametersOf(id) }
        )
        val roomInfoStore by inject<Store<RoomInfo.Intent, RoomInfo.State, RoomInfo.Label>>(
            named(StoreName.RoomInfo),
            parameters = { parametersOf(id) }
        )
    }

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @Composable
    override fun Content() {
        val scope = rememberScreenModel { ScreenScope(id) }
        val state by scope.timelineStore.states.collectAsState(initial = scope.timelineStore.state)

        Scaffold(
            topBar = {
                RoomInfoAppBar(scope.roomInfoStore)
            },
            content = {
                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    reverseLayout = true
                ) {
                    fun loadMore(index: Int) {
                        if (!state.loading) {
                            if (index >= state.items.lastIndex - 5) {
                                scope.timelineStore.accept(Timeline.Intent.LoadMoreBefore)
                            } else if (index <= 5) {
                                scope.timelineStore.accept(Timeline.Intent.LoadMoreAfter)
                            }
                        }
                    }

                    state.items.forEachIndexed { i, item ->
                        when (item) {
                            is MembersPack -> item {
                                loadMore(i)
                                Text(
                                    modifier = Modifier
                                        .padding(8.dp)
                                        .fillMaxWidth(),
                                    text = "members: ${item.events.size}",
                                    textAlign = TextAlign.Center
                                )
                            }

                            is MessagesPack -> {
                                val my = item.senderId == scope.currentUserId
                                itemsIndexed(item.events) { index: Int, event: TimelineEvent ->
                                    loadMore(i)
                                    val first = index == 0
                                    val last = index == item.events.lastIndex
                                    val shape = RoundedCornerShape(
                                        topStart = if (last) 8.dp else 0.dp,
                                        topEnd = if (last) 8.dp else 0.dp,
                                        bottomStart = if (first) {
                                            if (my) 8.dp else 0.dp
                                        } else 0.dp,
                                        bottomEnd = if (first) {
                                            if (my) 0.dp else 8.dp
                                        } else 0.dp
                                    )
                                    val externalPaddings = PaddingValues(
                                        start = if (my) 16.dp else 4.dp,
                                        top = if (last) 4.dp else 0.dp,
                                        end = if (my) 4.dp else 16.dp,
                                        bottom = if (first) 4.dp else 1.dp,
                                    )
                                    Row(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(externalPaddings)
                                    ) {
                                        if (!my) {
                                            Box(
                                                Modifier.width(40.dp).align(Alignment.Bottom)
                                            ) {
                                                if (first) {
                                                    val member = remember(state.members) { state.members[item.senderId] }
                                                    MemberAvatar(
                                                        member = member,
                                                        modifier = Modifier
                                                            .height(40.dp)
                                                            .width(40.dp)
                                                            .background(MaterialTheme.colors.secondary)
                                                            .background(MaterialTheme.colors.background, RoundedCornerShape(bottomEnd = 8.dp))
                                                            .padding(4.dp)
                                                            .offset(x = (-2).dp, y = 4.dp)
                                                    )
                                                }
                                            }
                                        }
                                        Text(
                                            modifier = Modifier
                                                .weight(0.1f)
                                                .heightIn(min = 40.dp)
                                                .background(MaterialTheme.colors.secondary, shape)
                                                .padding(8.dp)
                                                .align(Alignment.CenterVertically),
                                            text = (event.content as EventContent.RoomMessage).body ?: "Message removed",
                                            color = MaterialTheme.colors.onSecondary
                                        )
                                        if (my) {
                                            Box(
                                                Modifier.width(40.dp).align(Alignment.Bottom)
                                            ) {
                                                if (first) {
                                                    val member = remember(state.members) { state.members[item.senderId] }
                                                    MemberAvatar(
                                                        member = member,
                                                        modifier = Modifier
                                                            .height(40.dp)
                                                            .width(40.dp)
                                                            .background(MaterialTheme.colors.secondary)
                                                            .background(MaterialTheme.colors.background, RoundedCornerShape(bottomStart = 8.dp))
                                                            .padding(4.dp)
                                                            .offset(x = 2.dp, y = 4.dp)
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            is TimelineEvent -> item {
                                loadMore(i)
                                Text(
                                    modifier = Modifier
                                        .padding(8.dp)
                                        .fillMaxWidth(),
                                    text = item.type,
                                    textAlign = TextAlign.Center
                                )
                            }
                        }
                    }

                    if (state.loading) {
                        item {
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(4.dp)
                                    .height(24.dp)
                            ) {
                                CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
                            }
                        }
                    }
                }
            }
        )
    }
}

@Composable
private fun MemberAvatar(
    member: Member?,
    modifier: Modifier = Modifier
) {
    Box(
        modifier = modifier
            .background(MaterialTheme.colors.secondary, CircleShape)
    ) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            color = MaterialTheme.colors.onSecondary,
            fontSize = 12.sp,
            style = LocalTextStyle.current.merge(
                @Suppress("DEPRECATION")
                (TextStyle(
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false
                    ),
                    lineHeightStyle = LineHeightStyle(
                        alignment = LineHeightStyle.Alignment.Center,
                        trim = LineHeightStyle.Trim.None
                    )
                ))
            ),
            text = (member?.name ?: member?.id).orEmpty().take(2).uppercase()
        )
        val url = member?.avatarUrl
        if (url != null) {
            AsyncImage(
                modifier = Modifier
                    .fillMaxSize()
                    .clip(CircleShape),
                model = App.convertMediaUrl(url),
                contentDescription = null
            )
        }
    }
}
