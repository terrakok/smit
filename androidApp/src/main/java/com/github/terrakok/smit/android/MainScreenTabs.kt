package com.github.terrakok.smit.android

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.OtherHouses
import androidx.compose.material.icons.filled.Sms
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import com.github.terrakok.smit.app.rooms.RoomList
import com.github.terrakok.smit.core.SmitService
import org.koin.androidx.compose.get

object HomeScreen : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val title = "Home"
            val icon = rememberVectorPainter(Icons.Filled.OtherHouses)
            return remember { TabOptions(0u, title, icon) }
        }

    @Composable
    override fun Content() {
        Navigator(RoomListScreen(RoomList.ROOT_ROOM_ID))
    }
}

object DirectRoomsScreen : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val title = "Direct messages"
            val icon = rememberVectorPainter(Icons.Filled.Sms)
            return remember { TabOptions(1u, title, icon) }
        }

    @Composable
    override fun Content() {
        Navigator(RoomListScreen(RoomList.DIRECT_ROOMS_ID))
    }
}

object ProfileScreen : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val title = "Profile"
            val icon = rememberVectorPainter(Icons.Filled.AccountCircle)
            return remember { TabOptions(2u, title, icon) }
        }

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @Composable
    override fun Content() {
        val smitService = get<SmitService>()
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text("Profile") }
                )
            }
        ) {
            Box(
                modifier = Modifier.fillMaxSize()
            ) {
                Button(
                    modifier = Modifier.align(Alignment.Center),
                    onClick = { smitService.logout() }
                ) {
                    Text("Logout")
                }
            }
        }
    }
}